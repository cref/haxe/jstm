/**
 * ...
 * @author Cref
 */

package jstm;

#if macro
import haxe.macro.JSGenApi;

class HostGenerator extends JScriptHostGenerator__ {

	override public function writeSplit(folderPath:String, ext:String):Array<String> {
		var filenames = super.writeSplit(folderPath, ext);
		writeFile(folderPath + '/' + mainClassName + '.wsf',
			'<?xml version="1.0" encoding="UTF-8"?>\n'+
			'<job>\n'+
			includeTemplate[0] + filenames.join(includeTemplate[1] + '\n' + includeTemplate[0]) + includeTemplate[1]+
			'\n<script language="jscript">jstm("' + mainClassName + '")</script>\n' +
			'</job>'
		);
		return filenames;
	}
	
	static var includeTemplate=['<script language="jscript" src="','"></script>'];
	
}
#end