﻿/**
* ...
* @author cref
*/
import activex.TextStream;

//http://www.microsoft.com/technet/scriptcenter/guide/sas_wsh_qlcc.mspx?mfr=true
extern class WScript {
	public static function sleep(msec:Int):Void;
	public static function echo(str:String):Void;
	public static function connectObject(script:Script,prefix:String):Void;
	public static function disconnectObject(script:Script):Void;
	public static function createObject(progId:String,?prefix:String):Dynamic;
	public static function getObject(path:String,?progId:String,?prefix:String):Dynamic;
	public static function quit():Void;
	public static var stdErr(default,null):TextStream;
	public static var stdIn(default,null):TextStream;
	public static var stdOut(default,null):TextStream;
	public static var arguments(default,null):Args;
	public static var version(default,null):String;
	public static var buildVersion(default,null):Int;
	public static var fullName(default,null):String;
	public static var Name(default,null):String;//may not be lowercase, should be allowed by haXe compiler...
	public static var path(default,null):String;
	public static var scriptName(default,null):String;
	public static var scriptFullName(default,null):String;
	public static var interactive:Bool;
}

//TODO: integrate with Collection?
typedef CList = {
	var length(default,null):Int;
}

typedef Unnamed = {>CList,
	function item(index:Int):String;
}

typedef Named = {>CList,
	function exists(name:String):Bool;
	function item(name:String):String;
}

typedef Args = {>Unnamed,
	var named(default, null):Named;
	var unnamed(default, null):Unnamed;
	function showUsage():Void;
}

//TODO:
typedef Script = {
	function execute():Void;
}
