﻿/**
 * 
* @author cref
*/
package activex.wscript;

extern class Controller {
	public function new():Void;
  public function createScript(scriptName:String,hostName:String):WScript.Script;
}