/**
 * ...
 * @author Cref
 */

package js;

class Lib {

	/**
		Print the specified value on the default output.
	**/
	public static inline function print( v : Dynamic ) : Void WScript.stdOut.write(v)

	/**
		Print the specified value on the default output followed by a newline character.
	**/
	public static inline function println( v : Dynamic ) : Void WScript.stdOut.writeLine(v)
	
}