/**
 * Image magick ActiveX object
 * requires DLL
 */
package activex.imagemagickobject;

extern class MagickImage {
	
	public function new():Void;
	/**
	 * Calls the convert CLI with the parts space-seperated
	 * @return	Response message (command line response)
	 */
	public function convert(
		p0 : String, 
		?p1 : String, 
		?p2 : String, 
		?p3 : String, 
		?p4 : String, 
		?p5 : String, 
		?p6 : String, 
		?p7 : String, 
		?p8 : String, 
		?p9 : String, 
		?p10: String, 
		?p11: String, 
		?p12: String, 
		?p13: String, 
		?p14: String, 
		?p15: String, 
		?p16: String, 
		?p17: String) : String;
		
}