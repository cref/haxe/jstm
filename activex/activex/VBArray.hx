/**
 * ...
 * @author Cref
 */

package activex;

typedef VBArray<T> = {
	function toArray():Array<T>;
	function dimensions():Int;
	function getItem(dim0:Int, ?dim1:Int, ?dim2:Int, ?dim3:Int, ?dim4:Int, ?dim5:Int, ?dim6:Int, ?dim7:Int, ?dim8:Int, ?dim9:Int):T;
	function lbound(dimension:Int):Int;
	function ubound(dimension:Int):Int;
}