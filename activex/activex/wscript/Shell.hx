﻿/**
* @author cref
*/

package activex.wscript;

import activex.Collection;
import activex.scripting.FileSystemObject;
import activex.TextStream;

extern class Shell {
	public function new():Void;
	/*
	Properties
	*/
	var currentDirectory:String;
	var specialFolders(default,null):Collection<Folder>;
	/*
	Methods
	*/
	//processId mag zowel String als Int zijn, hoe geef ik dit aan in haXe?
	function appActivate(processId:String):Void;
	//retourneert een Shortcut OF een UrlShortcut, hoe aan te geven?
	function createShortcut():Shortcut;
	//USER, SYSTEM, VOLATILE, PROCESS
	//http://technet.microsoft.com/en-us/library/ee156595.aspx
	function environment(name:String):WritableAssociativeCollection<String>;
	//user moet uitvoer recht hebben
	function exec(command:String):ScriptExec;
	function expandEnvironmentStrings(envVar:String):String;
	function logEvent(intType:Int,strMessage:String,?strTarget:String):Void;
	//TODO: enum maken voor constants: http://msdn.microsoft.com/en-us/library/x83z1d9f(VS.85).aspx
	function popup(strText:String,?nSecondsToWait:Int,?strTitle:String,?nType:Int):Int;
	function regDelete(strName:String):Void;
	function regRead(strName:String):String;
	//anyValue moet waarschijnlijk Dynamic zijn
	function regWrite(strName:String,anyValue:String,?strType:String):Void;
	//retourneert errorcode
	function run(strCommand:String,?intWindowStyle:Int,?bWaitOnReturn:Bool):Int;
	//zie http://msdn.microsoft.com/en-us/library/8c6yea83(VS.85).aspx
	//voor speciale codes
	function sendKeys(keys:String):Void;
}

typedef ScriptExec={
	/*
	Properties
	*/
	var exitCode(default,null):Int;
	var processID(default,null):Int;
	var status(default,null):Int;
	var stdErr(default,null):TextStream;
	var stdIn(default,null):TextStream;
	var stdOut(default,null):TextStream;
	/*
	Methods
	*/
	function terminate():Void;
}
//TODO:
typedef Shortcut = { }
//typedef UrlShortcut = { > Shortcut}
