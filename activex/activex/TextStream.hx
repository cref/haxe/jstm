/**
 * ...
 * @author Cref
 */

package activex;

typedef TextStream = {
	/*
	Properties
	*/
	//Returns a Boolean value. If the file pointer is positioned immediately before the file's end-of-line marker, the value is True, and False otherwise. 
	var atEndOfLine(default,null):Bool;
	//Returns a Boolean value. If the file pointer is positioned at the end of a file, the value is True, and False otherwise.
	var atEndOfStream(default,null):Bool;
	//Returns the current position of the file pointer within the current line. Column 1 is the first character in each line. 
	var column(default,null):Int;
	//This property returns the current line number in a text file.
	var line(default, null):Int;
	/*
	Methods
	*/
	//Closes a currently open TextStream file.
	function close():Void;
	//This method reads the number of characters you specify from a Textstream file and returns them as a string.
	function read(characters:Int):String;
	//This method reads the entire contents of a text file and returns it as a string.
	function readAll():String;
	//Reads a single line (excluding the newline character) from a TextStream file and returns the contents as a string. 
	function readLine():String;
	//Causes the file pointer to skip ahead a specified number of characters when reading a TextStream file.
	function skip(nchars:Int):Void;
	//Moves the file pointer from its current position to the beginning of the next line.
	function skipLine():Void;
	//This method writes a supplied string to an open TextStream file.
	function write(str:String):Void;
	//Used to write a number of consecutive newline characters (defined with the lines parameter) to a TextStream file.
	function writeBlankLines(nlines:Int):Void;
	//Writes a supplied string to a TextStream file, followed by a new line character.
	function writeLine(?s:String):Void;
}