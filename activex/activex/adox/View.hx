package activex.adox;

/**
 * View in an ADOX schema catalog
 */
extern class View {
	
	public function new():Void;
	
	//Specifies an ADO Command object that can be used to create or execute the procedure.
	public var command : activex.adodb.Command;
	
	//Returns a Variant value specifying the date created. The value is null if DateCreated is not supported by the provider.
	public var dateCreated(default, null) : Dynamic;

	//Returns a Variant value specifying the date modified. The value is null if DateModified is not supported by the provider.
	public var dateModified(default, null) : Dynamic;
	
	//Indicates the name of the object.
	public var name : String;
	
}