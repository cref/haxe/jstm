package activex.adox;

import activex.adodb.Collection;
import activex.adodb.Connection;

/**
 * Contains collections (Tables, Views, Users, Groups, and Procedures) that describe the schema catalog of a data source.
 */
extern class Catalog {
	
	function new():Void;
/*
	Properties
*/
	//Sets a Connection object or a String containing the definition for a connection. Returns the active Connection object.
	public var activeConnection(default, default) : Connection;

	//Contains all Table objects of a catalog.
	public var tables(default, null) : Collection<Table>;
	
	//Contains all Group objects of a catalog.
	public var groups(default, null) : Collection<Group>;

	//Contains all User objects of a catalog.
	public var users(default, null) : Collection<User>;

	//Contains all Procedure objects of a catalog.
	public var procedures(default, null) : Collection<Procedure>;

	//Contains all View objects of a catalog.
	public var views(default, null) : Collection<View>;

	/**
	 * The Create method creates and opens a new ADO Connection to the data source specified in ConnectString. If successful, the new Connection object is assigned to the ActiveConnection property.
	 * @param	conn
	 * @note An error will occur if the provider does not support creating new catalogs.
	 */
	public static function create(conn : String) : Catalog;
	
	/**
	 * Returns a String value that specifies the Name of the User or Group that owns the object.
	 * @param	objectName		A String value that specifies the name of the object for which to return the owner.
	 * @param	objectType		A Long value which can be one of the ObjectTypeEnum constants, that specifies the type of the object for which to get the owner.
	 * @param	?objectTypeId		A Variant value that specifies the GUID for a provider object type not defined by the OLE DB specification. This parameter is required if ObjectType is set to adPermObjProviderSpecific; otherwise, it is not used.
	 */
	public function getObjectOwner(objectName : String, objectType : Int, ?objectTypeId : Int) : String;

	/**
	 * Sets the Name of the User or Group that owns the object.
	 * @param	objectName		A String value that specifies the name of the object for which to return the owner.
	 * @param	objectType		A Long value which can be one of the ObjectTypeEnum constants, that specifies the type of the object for which to get the owner.
	 * @param	?objectTypeId		A Variant value that specifies the GUID for a provider object type not defined by the OLE DB specification. This parameter is required if ObjectType is set to adPermObjProviderSpecific; otherwise, it is not used.
	 */
	public function setObjectOwner(objectName : String, objectType : Int, ?objectTypeId : Int) : Void;
	
}