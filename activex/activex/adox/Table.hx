package activex.adox;

/**
 * Table in an ADOX schema catalog
 */
extern class Table {

	//Creates a new table
	function new():Void;
	
	//Indicates the name of the table.
	public var name : String;

	//Returns a String value that specifies the type of table; for example, "TABLE", "SYSTEM TABLE", or "GLOBAL TEMPORARY".
	public var type(default, null) : String;
	
	//Specifies the parent catalog of a table or column to provide access to provider-specific properties.
	public var parentCatalog : Catalog;

	//Returns a Variant value specifying the date created. The value is null if DateCreated is not supported by the provider.
	public var dateCreated(default, null) : Dynamic;
	
	//Returns a Variant value specifying the date modified. The value is null if DateModified is not supported by the provider.
	public var dateModified(default, null) : Dynamic;

	//Contains all Column objects of the table.
	public var columns(default, null) : activex.adodb.Collection<Column>;
	
	//Contains all Index objects of the table.
	public var indexes(default, null) : activex.adodb.Collection<Index>;
	
	//Contains all Key objects of a table.
	public var keys(default, null) : activex.adodb.Collection<Key>;
	
	//Contains all the properties of the table.
	public var properties(default, null) : activex.adodb.PropertiesCollection;

}