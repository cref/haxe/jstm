package activex.adox;

/**
 * Column in an ADOX schema catalog table
 */
extern class Column {

	//Creates a new column
	function new():Void;
	
	//Sets or returns a Long value. The value specifies characteristics of the table represented by the Column object and can be a combination of ColumnAttributesEnum constants. The default value is zero (0), which is neither adColFixed nor adColNullable.
	public var attributes : Int;
	
	//Sets and returns a Long value that is the maximum length in characters of data values.
	public var definedSize : Int;
	
	//Indicates the name of the column
	public var name : String;
	
	//Sets and returns a Byte value that is the scale of data values in the column when the Type property is adNumeric or adDecimal. NumericScale is ignored for all other data types.
	public var numericScale : Int;
	
	//Specifies the parent catalog of a table or column to provide access to provider-specific properties.
	public var parentCatalog : Catalog;

	//Sets and returns a Long value that is the maximum precision of data values in the column when the Type property is a numeric type. Precision is ignored for all other data types.
	public var precision : Int;

	//Indicates the name of the related column in the related table (key columns only).
	public var relatedColumn : String;
	
	//Sets and returns a Long value that can be one of the SortOrderEnum constants. The default value is adSortAscending.
	public var sortOrder : Int;
	
	//Sets or returns a Long value that can be one of the DataTypeEnum constants. The default value is adVarWChar.
	public var type : Int;

	//Contains all the properties of the column.
	public var properties(default, null) : activex.adodb.PropertiesCollection;
	
}