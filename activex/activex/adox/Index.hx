package activex.adox;

/**
 * Index in an ADOX schema catalog table
 */
extern class Index {

	//Creates a new table
	function new():Void;
	
	//Indicates whether the index is clustered.
	public var clustered : Bool;
	
	//Indicates the name of the index.
	public var name : String;

	//Contains all Column objects of index.
	public var columns(default, null) : activex.adodb.Collection<Column>;
	
	//Indicates whether records that have null values in their index fields have index entries.
	public var indexNulls : Bool;
	
	//Indicates whether the index represents the primary key on the table.
	public var primaryKey : Bool;
	
	//Indicates whether the index keys must be unique.
	public var unique : Bool;

	//Contains all the properties of the index.
	public var properties(default, null) : activex.adodb.PropertiesCollection;
	
}