/**
 * For lots of useful examples, go here: http://www.paulsadowski.com/wsh/cdo.htm
 * @author Cref
 */

package activex.cdo;

extern class Message {
	function new():Void;
  function send():Void;
	//can also be local: "file://c|/temp/test.htm"
	function createMHTMLBody(url:String):Void;
	function addAttachment(path:String):Void;
  var subject:String;
  var from:String;
  var to:String;
  var cc:String;
  var bcc:String;
  var textBody:String;
  var htmlBody:String;
  var configuration:Configuration;
}