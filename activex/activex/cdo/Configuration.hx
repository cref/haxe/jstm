/**
 * Adds strong-typed support for configuration fields through inlining.
 * @author Cref
 */

package activex.cdo;

import activex.adodb.Collection;
import activex.adodb.Field;

extern class Configuration {
	
	function new():Void;
	var fields:Collection<Field>;
	
	public inline var sendUsing(getSendUsing, setSendUsing):SendUsing;
	private inline function getSendUsing():SendUsing return Helper.cdoVal(this,'sendusing')
	private inline function setSendUsing(value:SendUsing):SendUsing return Helper.cdoVal(this,'sendusing', value)
	
	public inline var pickupFolder(getPickupFolder, setPickupFolder):String;
	private inline function getPickupFolder():String return Helper.cdoVal(this,'smtpserverpickupdirectory')
	private inline function setPickupFolder(value:String):String return Helper.cdoVal(this,'smtpserverpickupdirectory', value)
	
	public inline var sendUsername(getSendUsername, setSendUsername):String;
	private inline function getSendUsername():String return Helper.cdoVal(this,'sendusername')
	private inline function setSendUsername(value:String):String return Helper.cdoVal(this,'sendusername', value)
	
	public inline var sendPassword(getSendPassword, setSendPassword):String;
	private inline function getSendPassword():String return Helper.cdoVal(this,'sendpassword')
	private inline function setSendPassword(value:String):String return Helper.cdoVal(this,'sendpassword', value)
	
	public inline var smtpServer(getSMTPServer, setSMTPServer):String;
	private inline function getSMTPServer():String return Helper.cdoVal(this,'smtpserver')
	private inline function setSMTPServer(value:String):String return Helper.cdoVal(this,'smtpserver', value)
	
	public inline var smtpServerPort(getSMTPServerPort, setSMTPServerPort):Int;
	private inline function getSMTPServerPort():Int return Helper.cdoVal(this,'smtpserverport')
	private inline function setSMTPServerPort(value:Int):Int return Helper.cdoVal(this,'smtpserverport', value)
	
	public inline var smtpConnectionTimeout(getSMTPConnectionTimeout, setSMTPConnectionTimeout):Int;
	private inline function getSMTPConnectionTimeout():Int return Helper.cdoVal(this,'smtpsconnectiontimeout')
	private inline function setSMTPConnectionTimeout(value:Int):Int return Helper.cdoVal(this,'smtpconnectiontimeout', value)
	
	public inline var smtpAuthenticate(getSMTPAuthenticate, setSMTPAuthenticate):SMTPAuthenticate;
	private inline function getSMTPAuthenticate():SMTPAuthenticate return Helper.cdoVal(this,'smtpauthenticate')
	private inline function setSMTPAuthenticate(value:SMTPAuthenticate):SMTPAuthenticate return Helper.cdoVal(this,'smtpauthenticate', value)
	
	public inline var smtpUseSSL(getSMTPUseSSL, setSMTPUseSSL):Bool;
	private inline function getSMTPUseSSL():Bool return Helper.cdoVal(this,'smtpusessl')
	private inline function setSMTPUseSSL(value:Bool):Bool return Helper.cdoVal(this,'smtpusessl', value)
	
}

extern class SendUsing {

	public static inline var pickup:SendUsing = cast 1;
	public static inline var port:SendUsing = cast 2;
	
}

extern class SMTPAuthenticate {

	public static inline var anonymous:SMTPAuthenticate = cast 1;
	public static inline var basic:SMTPAuthenticate = cast 2;
	public static inline var ntlm:SMTPAuthenticate = cast 3;
	
}

/**
 * Shortcut for reading and writing CDO message configuration parameters.
 * Also updates the fields collection after changing a value.
 * @author Cref
 */
private class Helper {
	public static function cdoVal(cfg:Configuration, field:String, ?value:Dynamic):Dynamic {
		var f = cfg.fields.item("http://schemas.microsoft.com/cdo/configuration/" + field);
		if (ES5.arguments.length > 2 && f.value != value) {
			f.value = value;
			cfg.fields.update();
		}
		return f.value;
	}
}