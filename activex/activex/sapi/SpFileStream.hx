﻿/**
* ...
* @author cref
*/

package activex.sapi;

/*
http://msdn.microsoft.com/en-us/library/ms722561(VS.85).aspx
*/
extern class SpFileStream {
	function new():Void;
	function open(filename:String,?FileMode:Int,?DoEvents:Bool):Void;
}