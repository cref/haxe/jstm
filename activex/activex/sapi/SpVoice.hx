﻿/**
* ...
* @author cref
*/

package activex.sapi;

/*
http://msdn.microsoft.com/en-us/library/ms723602(VS.85).aspx
*/
extern class SpVoice {
	function new():Void;
	function speak(text:String,?flags:Int):Int;
	var audioOutputStream:SpFileStream;
}
