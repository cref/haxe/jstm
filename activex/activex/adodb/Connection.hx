package activex.adodb;

import activex.VBArray;
import activex.Collection;

extern class Connection {
	
	function new():Void;
/*
	Properties
*/
	//Sets or returns a long value defining the characteristics of a Connection object.
	public var attributes:Int;
	//Sets the number of seconds to wait while attempting an Execute method call before terminating the attempt and generating an error message.
	public var commandTimeout:Int;
	//Sets or returns a string value that contains the details used to create a connection to a data source.
	public var connectionString:String;
	//Sets the number of seconds to wait while attempting to create a connection before terminating the attempt and generating an error message.
	public var connectionTimeout:Int;
	//Sets or returns a long value used to select between various cursor libraries accessible through the provider.
	public var cursorLocation:Int;
	//Sets or returns a string value that is the default name of the database available from the provider for a Connection object.
	public var defaultDatabase:String;
	//Sets or returns the transaction isolation level (the IsolationLevelEnum value) of a Connection object.
	public var isolationLevel:Int;
	//Sets or returns the provider access permission (the ConnectModeEnum value) for a Connection object.
	public var mode:Int;
	//Sets or returns the string value that is the provider name.
	public var provider:String;
	//Returns a long value (the ObjectStateEnum value) describing if the connection is open or closed.
	public var state(default,null):Int;
	//Returns a string value that is the ADO version number.
	public var version(default,null):String;
	//The Errors Collection contains all of the Error objects that were created as the result of a single failure involving the provider. Each time a failure occurs involving the provider, the Errors Collection is cleared and the new Error objects that have been created are inserted into the collection.
	public var errors(default, null):Collection<Error>;
	//The Properties Collection is a collection of Property objects. Each Property object contains a single piece of information, called a dynamic property, about the database provider. By referring to the Properties Collection, each connection to a provider can be tailored specifically by ADO to suit the exact needs of that provider.
	public var properties(default, null):PropertiesCollection;
	/*
	Methods
	*/
	//Begins a new transaction and returns a long value indicating the number of nested transactions.
	public function beginTrans():Void;
	//Cancels the execution of a pending Execute or Open call.
	public function cancel():Void;
	//Closes a connection.
	public function close():Void;
	//Saves any changes and ends the current transaction. It can also be set to automatically start a new transaction.
	public function commitTrans():Void;
	//Executes the query, SQL statement, stored procedure, or provider-specific text.
	public function execute(commandText:String,?recordsAffected:Int,?options:Int):RecordSet;
	//Opens a physical connection to a data source.
	public function open(connectionString:String,?userID:String,?password:String,?options:Int):Void;
	//Returns descriptive schema information from the provider about the data source.
	public function openSchema(queryType:Int,?criteria:VBArray<Dynamic>,?schemaID:String):RecordSet;
	//Cancels any changes that have occurred during the current transaction and then ends the transaction. It can also be set to automatically start a new transaction.
	public function rollbackTrans():Void;
	/*
	* ADO events don't work from scripting
	* */
}