package activex.adodb;

/**
 * A single field in a ADO recordset result record
 */
extern class Field {
	
	//Returns a long value that is the actual length of a Field object's value.
	public var actualSize(default, null) : Int;

	//Returns a long value that is the sum of one or more FieldAttributeEnum values that define the characteristics of a Field object.
	public var attributes(default, null) : Int;

	//Returns a long value that is the defined (maximum possible) size (data capacity) of a Field object.
	public var definedSize(default, null) : Int;

	//Sets or returns a string value that is the name of the Field object.
	public var name(default, default) : String;

	//Sets or returns a byte value that is the number of digits allowed to the right of the decimal point for a numeric Field object.
	public var numbericScale(default, default) : Int;

	//Returns a variant that is the value of a field in the database before any changes were made in the current session.
	public var originalValue(default, null) : Dynamic;

	//Sets or returns a byte value that is the maximum number of digits allowed in a numeric Field object.
	public var precision(default, default) : Int;

	//Returns a FieldStatusEnum value that allows you to determine if a field has been successfully added.
	public var status(default, null) : Int;

	//Sets or returns a DataTypeEnum value that specifies the data type.
	public var type(default, default) : Int;

	//Returns a variant that is the current field value as stored in the database.
	public var underlyingValue(default, null) : Dynamic;

	//Returns a variant that is the current (visible) field value in the current recordset.
	//public var value(default, null) : Dynamic;
	public var value : Dynamic;

	//Collection of Property objects. Each Property object contains a provider-specific property. 
	public var properties(default, null) : PropertiesCollection;
	
	//Used to append a large amount (i.e., a large chunk) of text or binary data to a Field object.
	public function appendChunk(data : Dynamic) : Void;

	//Returns a variant that contains the specified amount (size) of binary or text data.
	public function getChunk(size : Int) : Dynamic;

}