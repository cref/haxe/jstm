package activex.adodb;
extern class Property {
	//Returns a long value that is the sum of one or more FieldAttributeEnum valuesthat define the characteristics of a Property object.
	public var attributes:Int;
	//Sets or returns a string value that is the name of the Property object.
	public var name:String;
	//Sets or returns a DataTypeEnum value that specifies the data type.
	public var type:Int;
	//Sets or returns a variant that is the value of the Property object. 
	public var value:Dynamic;
}