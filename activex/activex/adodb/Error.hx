package activex.adodb;

//extends org.ecmascript.Error?
extern class Error {
	
	public function new():Void;
	//Returns a string that describes the error.
	//unfortunately, an adodb.Error has no message property as is standard.
	public var description(default,null):String;
	//Returns a long value that is the context ID in the help file (if it exists) for the error.
	public var helpContext(default,null):Int;
	//Returns a string that is the path and name of the help file (if it exists).
	public var helpFile(default,null):String;
	//Returns a long value that is the database error information for a specific Error object.
	public var nativeError(default,null):Int;
	//Returns a long value that is the unique number that identifies an Error object.
	public var number(default,null):Int;
	//Returns a string that is the name or ID of the object or application that generated the error.
	public var source(default,null):String;
	//Returns a five character string that is the SQL error code.
	public var sqlState(default, null):String;
}