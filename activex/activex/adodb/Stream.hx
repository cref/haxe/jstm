package activex.adodb;

import activex.ByteArray;

extern class Stream {
	
	//make sure the stream.lineSeparator is set to the correct value first! (10 for utf-8)
	inline function readLine():String return readText(-2)
	
	public function new():Void;
	
	//Sets or returns a value that specifies into which character set the contents are to be translated. This property is only used with text Stream objects (type is adTypeText)
	public var charset : String;
	
	//Returns a Boolean value indicating whether or not the current position is at the end of the stream. 
	public var eos(default, null) : Bool;
	
	//Sets or returns a LineSeparatorEnum value that specifies which binary character to use as the line separator in a text Stream object. (13 = CR, 10 = LF or -1 = Both)
	public var lineSeparator : Int;

	//Sets or returns a ConnectModeEnum value that specifies the available permissions for modifying data. 
	public var mode : Int;
	
	//Sets or returns a long value that specifies the current position, measured in bytes, from the beginning of the stream. 
	public var position : Int;
	
	//Returns a long value that is the size in bytes of an opened Stream object. 
	public var size(default, null) : Int;
	
	//Returns a long value describing if the Stream object is open or closed.
	public var state(default, null) : Int;
	
	//Sets or returns a StreamTypeEnum value defining if the data is binary or text. 
	public var type : Int;
	
	//Cancels the execution of a pending Open call.
	public function cancel() : Void;

	//Close a Stream object.
	public function close() : Void;

	//Copies the specified number of characters or bytes from one Stream object to another Stream object.
	public function copyTo(destStream : Stream, numChars : Int) : Void;

	//Sends the contents of the Stream object to the underlying object that is the source of the Stream object.
	public function flush() : Void;

	//Loads the contents of an existing file into an open Stream object.
	public function loadFromFile(filename : String) : Void;

	/**
	 * Opens a Stream object from a URL or Record object.
	 * @param	?source			A variant that indicates the source of the data for the Stream object. This can be a URL or a reference to an already opened Record object. If you do not specify a source, a new Stream will be created and opened. It will have a Size of zero and will contain no data since it will not be associated with any underlying source. 
	 * @param	?mode			One of the ConnectModeEnum constants that dictate the access permissions for a Stream object. If the Source parameter is an already opened Record object, this parameter will be implicitly set. 	
	 * @param	?openOptions		A StreamOpenOptionsEnum constant that specifies possible options for opening a Stream object. 
	 * @param	?userName		The name of a user who can access the Stream object. If the Source parameter is an already opened Record, neither the UserName nor the Password parameter is used. 
	 * @param	?password		The password that validates the UserName parameter. If the Source parameter is an already opened Record, neither the UserName nor the Password parameter is used. 
	 */
	public function open(?source : Dynamic, ?mode : Int, ?openOptions : Int, ?userName : String, ?password : String) : Void;

	//Reads the specified number of bytes from a binary Stream object and returns the data as a variant.
	public function read(?numBytes : Int) : ByteArray;

	/**
	 * Reads the specified number of bytes from a text Stream object and returns the data as a string.
	 * @param	?numChars		Either the number of characters to read or one of the StreamReadEnum constants. If you specify a number larger than the actual number of characters available in the Stream, only the actual available characters are read and no error is generated. A null value is returned if there are no characters left to be read. 
	 * @return
	 */
	public function readText(?numChars : Int) : String;

	/**
	 * Copies (saves) the contents of an opened Stream object to a specified file.
	 * @param	fileName		The name of the file (including the path, if needed) into which the data will be saved. This can be the name of an existing file or a new file you wish to create. 	
	 * @param	?saveOptions		 One of the SaveOptionsEnum constants that allow you to either overwrite an existing file or create a new file. If left blank, the default is to create a new file.
	 */
	public function saveToFile(fileName : String, ?saveOptions : Int) : Void;

	//Sets the value of the EOS property to be the current position.
	public function setEos() : Void;

	//Skips all of the characters on one entire line, including the next line separator, while reading a text stream.
	public function skipLine() : Void;

	//Writes a specified number of bytes of binary data to an opened Stream object without adding any intervening spaces.
	public function write(buffer : ByteArray) : Void;

	/**
	 * Writes a specified text string to an opened Stream object without adding any intervening spaces or characters. 	
	 * @param	data		Contains the text data to write to the Stream.
	 * @param	?options		One of the StreamWriteEnum constants that determines whether or not a line separator is added to the end of the written text. The LineSeparator property must be set if you wish to add a line separator, or a run-time error will occur. 
	 */
	public function writeText(data : String, ?options : Int) : Void;
	
}