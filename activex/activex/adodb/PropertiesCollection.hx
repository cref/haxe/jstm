/**
 * ...
 * @author Cref
 */

package activex.adodb;

import activex.Collection;

extern class PropertiesCollection extends AssociativeCollection<Property> {
	/**
	 * Updates the Property objects in the Properties Collection with the dynamic property information specific to the provider.
	 * It is quite possible that the provider has dynamic properties that are not supported by ADO.
	 */
	public function refresh() : Void;

}