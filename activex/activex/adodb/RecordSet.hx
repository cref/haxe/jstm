package activex.adodb;

import activex.VBArray;
/*
* TODO:
* Make enums for constants:
* http://www.eggheadcafe.com/PrintSearchContent.asp?LINKID=122
* */
extern class RecordSet {
	
	public function new():Void;
	/*
	* Properties
	* */
	//Sets or returns a long value that is the current page number in the Recordset object,or the PositionEnum value (either BOF, EOF, or empty).
	public var absolutePage:Int;
	//Sets or returns a long value that is the ordinal position of the cursor.
	public var absolutePosition:Int;
	//Returns a variant that is the Command object associated with the Recordset object.
	public var activeCommand(default,null):Command;
	//Sets or returns a variant defining the Connection object to which the Recordset belongs, or returns a string value if there is no connection.
	public var activeConnection:Connection;
	//Returns a Boolean value indicating if the current record position is before the first record.
	public var bof(default,null):Bool;
	//Sets or returns a variant value that uniquely defines the position of a record in a recordset.
	public var bookmark:Bookmark;
	//Sets or returns a long value that is the number of records that are cached or are allowed to be cached.
	public var cacheSize:Int;
	//Sets or returns a long value that is a CursorLocationEnum value which defines the location of the cursor engine (server-side or client-side).
	public var cursorLocation:Int;
	//Sets or returns a CursorTypeEnum value that defines the type of cursor being used.
	public var cursorType:Int;
	//Sets or returns a string value that names the data member referenced by the DataSource property.
	public var dataMember:String;
	//Specifies the object containing the data member that will be represented as a Recordset object.
	public var dataSource:RecordSet;	//Returns an EditModeEnum value that defines the editing status of the current record.
	public var editMode(default,null):Int;
	//Returns a Boolean value indicating if the current record position is after the last record.
	public var eof(default,null):Bool;
	//Sets or returns a variant value that is either a string, array of bookmarks, or a FilterGroupEnum value used to filter data.You also use this property to turn an existing Filter off.
	public var filter:Dynamic;//String, VBArray<Bookmark>, Int
	//Sets or returns a string value that is the name of the index in effect.
	public var index:String;
	//Sets or returns a LockTypeEnum value that defines the type of locks that arein effect while editing records.
	public var lockType:Int;
	//Sets or returns a MarshalOptionEnum value that specifies which records are to betransferred (marshaled) back to the server.
	public var marshalOptions:Int;
	//Sets or returns a long value that specifies the maximum number of records that can be returned to a Recordset object as the result of a query.
	public var maxRecords:Int;
	//Returns a long value that is the number of pages contained in a Recordset object.
	public var pageCount(default,null):Int;
	//Sets or returns a long value that specifies how many records are on one page.
	public var pageSize:Int;
	//Returns a long value that is the count of how many records are in a Recordset object.
	public var recordCount(default,null):Int;
	//Sets or returns a string value that is a comma-delineated list of the names of which fields in the Recordset to sort.After each name, you can optionally add a blank space and the keywords ASC or DESC to designate the sort direction.
	public var sort:String;
	//Sets or returns a string value that defines the data source for a Recordset object.
	public var source(default,null):Int;
	//Returns a long value describing if the Recordset object is open, closed, or inthe process of connecting, executing, or retrieving.
	public var state(default,null):Int;
	//Returns a sum of one or more RecordStatusEnum values describing the status of the current record.
	public var status(default,null):Int;
	//For a hierarchical Recordset object, sets or returns a Boolean value defining whether the reference to the underlying child record, called the chapter, needs updating.
	public var stayInSync:Bool;
	//The Fields Collection is a collection of all of the Field objects associated with a specific Record object.
	public var fields(default,null):Collection<Field>;
	//The Properties Collection is a collection of Property objects. Each Property object contains a single piece of information, called a dynamic property, about the database provider. By referring to the Properties Collection, each connection to a provider can be tailored specifically by ADO to suit the exact needs of that provider.
	public var properties(default,null):PropertiesCollection;
	/*
	* Methods
	* */
	//Used to create a new record.
	public function addNew(?fieldList:Dynamic,?values:Dynamic):Void;
	//Cancels the execution of a pending Open call.
	public function cancel():Void;
	//Used to cancel a pending batch update.You must be in batch update mode.
	public function cancelBatch(?affectRecords:Int):Void;
	//Used to cancel any changes made to the current row or to cancel the addition of a new row to a Recordset.This must be done before performing an Update.
	public function cancelUpdate():Void;
	//Creates a duplicate copy of a Recordset object by copying an existing Recordset object.
	public function clone(?lockType:Int):RecordSet;
	//Closes a Recordset object.
	public function close():Void;
	//Returns a CompareEnum value that compares the relative rowposition of two bookmarks in the same Recordset object.
	public function compareBookmarks(bookmark1:Bookmark,bookmark2:Bookmark):Int;
	//Deletes the current record, a group of records, or all records.
	public function delete(?affectRecords:Int):Void;
	//Searches for a row in a Recordset that matches the given criteria.
	public function find(criteria:String,?skipRecords:Int,?searchDirection:Int,?start:Dynamic):Void;
	//Used to copy either all or a specified number of records into a two-dimensional array.
	public function getRows(?rows:Int,?start:Dynamic,?fields:Dynamic):Void;
	//Returns the specified Recordset as a string.
	public function getString(?stringFormat:Int,?numRows:Int,?columnDelimiter:String,?rowDelimiter:String,?nullExpr:String):String;
	//Moves the position of the current record pointer.
	public function move(numRecords:Int,?start:Dynamic):Void;
	//Moves the position of the current record pointer to the first record.
	public function moveFirst():Void;
	//Moves the position of the current record pointer to the last record.
	public function moveLast():Void;
	//Moves the position of the current record pointer forward to the next record.
	public function moveNext():Void;
	//Moves the position of the current record pointer back to the previous record.
	public function movePrevious():Void;
	//Clears the current Recordset object and returns the next Recordset object.
	public function nextRecordset(?recordsAffected:Int):RecordSet;
	//When used on a Recordset object, this opens a cursor that is used to navigate through records.
	public function open(?source:Dynamic,?activeConnection:Connection,?cursorType:Int,?lockType:Int,?options:Int):Void;
	//Used to update (refresh) the data in a Recordset object.This is essentially equivalent to a Close followed by an Open.
	public function requery(?options:Int):Void;
	//Refreshes the data in the current Recordset object by re-synchronizing records with the underlying (original) database.
	public function resync(?affectRecords:Int,?resyncValues:Int):Void;
	//Saves the Recordset to a file or Stream object.
	public function save(?destination:Dynamic,?persistFormat:Int):Void;
	//Uses the index of a Recordset to locate a specified row.
	public function seek(keyValues:VBArray<Dynamic>,seekOption:Int):Void;
	//Returns a Boolean value that indicates whether or not a Recordset object willsupport a specific type of functionality.
	public function supports(cursorOptions:Int):Bool;
	//Used to save any changes made to the current row of a Recordset object.
	public function update(?fields:Dynamic,?values:Dynamic):Bool;
	//Writes all pending batch updates to the underlying database.
	public function updateBatch(?affectRecords:Int):Void;
	/*
	* ADO events don't work from scripting
	* */
}
