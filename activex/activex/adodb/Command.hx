package activex.adodb;

extern class Command {
	
	public function new():Void;

	/**
	 * Cancels an execution of a method
	 */
	public function cancel() : Void;
	
	/**
	 * Creates a new Parameter object
	 * @param name  	Optional. The name of the Parameter object.
	 * @param type 	Optional. One of the DataTypeEnum constants that specifies the data type for the Parameter object. Default is adEmpty. If you choose a variable-length data type, you will also need to specify the Size parameter or the Size property. If you specify adDecimal or adNumeric data type, you must also set the NumericScale and the Precision properties of the Parameter object.
	 * @param direction 	Optional. One of the ParameterDirectionEnum constants that defines the direction of the Parameter object. Default is adParamInput.
	 * @param size 	Optional. Specifies the length of a variable-length data type, if such a type was declared in the Type parameter. Default is zero.
	 * @param value 	Optional. The value of the Parameter object
	 */
	public function createParameter(?name : String, ?type : Int, ?direction : Int, ?size : Int, ?value : Dynamic) : Parameter;
	
	/**
	 * Executes the query, SQL statement or procedure in the CommandText property
	 * @param ra  	 Optional. Returns the number of records affected by a query. For a row-returning query, use the RecordCount property of the Recordset object to count of how many records are in the object.
	 * @param parameters 	Optional. Parameter values passed with an SQL statement. Used to change, update, or insert new parameter values into the Parameters Collection.
	 * @param options 	Optional. Sets how the provider should evaluate the CommandText property. Can be one or more CommandTypeEnum or ExecuteOptionEnum values. Default is adCmdUnspecified. 
	 * @return
	 */
	public function execute(?ra : Dynamic, ?parameters : Dynamic, ?options : Int) : RecordSet;

	//Indicates the text of a command to be issued against a provider.
	public var commandText : String;
	
	//Contains all the Parameter objects of a Command Object
	public var parameters(default, null) : Dynamic;
	
	//Contains all the Property objects of a Command Object
	public var properties(default, null) : Dynamic;

	//Definition for a connection if the connection is closed, or the current Connection object if the connection is open
	public var activeConnection : Connection;
	
	//The number of seconds to wait while attempting to execute a command
	public var commandTimeout : Int;
	
	//The name of a Command object
	public var name : String;
	
	//A Boolean value that, if set to True, indicates that the command should save a prepared version of the query before the first execution
	public var prepared : Bool;
	
	//A value that describes if the Command object is open, closed, connecting, executing or retrieving data
	public var state(default, null) : Int;

}