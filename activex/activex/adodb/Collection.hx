package activex.adodb;

/**
 * Looks like AssociativeCollection but behaves like Collection.
 */
extern class Collection<T> extends activex.Collection<T> {

	/**
	 * The Append method is used to add (append) an object to the Collection. 
	 * @param	?name		The unique name of the new Field object being appended to the collection.
	 * @param	?type		One of the DataTypeEnum constants that defines the data type of the new Field.
	 * @param	?definedSize		The size in bytes or characters of the new Field. When DefinedSize exceeds 255 bytes, the field is treated as having variable length columns.
	 * @param	?attrib		One of the FieldAttributeEnum constants that specify the attributes of the new Field. 
	 * @param	?value		The value for the new Field. If this parameter is not provided, it will be set to null when the new Field is appended. 
	 */
	//public function append(?name:String, ?type:Int, ?definedSize:Int, ?attrib:Int, ?value:Dynamic) : Void;
	/**
	 * Adds a new item to the collection
	 * @param item		The object to append or the name of the group to create and append.
	 */
	public function append(value:T) : Void;
	
	/**
	 * Designates that a specified Field object is to be deleted from the Fields Collection. You must call the Update method of the Fields Collection to make the deletion. 
	 * @param	index		Either the name property or the ordinal position (index) in the collection of the Field object. 
	 */
	@:multitype public function delete(?key:String,?index : Int) : Void;
	//public inline function deleteAt(index : Int) : Void delete(untyped index)
	
	public function update() : Void;
	
  @:multitype public function item(?key:String,?index:Int):T;
	//public inline function itemAt(index : Int) : T return item(untyped index)
	
	/**
	 * Updates the objects in a collection to reflect objects available from, and specific to, the provider.
	 */
	public function refresh() : Void;
	
}
