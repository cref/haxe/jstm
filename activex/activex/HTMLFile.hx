/**
 * TODO!
 * 
 * @author Cref
 */
package activex;

import org.w3c.dom.html.Window;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NodeType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Document;
import org.w3c.dom.Text;
import org.w3c.dom.DocumentPosition;
import org.w3c.dom.DOMUserData;
import org.w3c.dom.DOMObject;
import org.w3c.dom.UserDataHandler;
typedef HTMLDocument=org.w3c.dom.html.HTMLDocument<Void,HTMLDocument,HTMLElement>;
typedef HTMLElement=org.w3c.dom.html.HTMLElement<HTMLDocument,HTMLElement>;

extern class HTMLFile implements HTMLDocument {
	public function new():Void;
	//Node
  var nodeName(default,never):String;
	var nodeValue:String;// raises(DOMException) on setting, raises(DOMException) on retrieval
  var nodeType(default,never):NodeType;
  var parentNode(default,never):HTMLElement;
  var childNodes(default,never):NodeList<HTMLElement>;
  var firstChild(default,never):HTMLElement;
  var lastChild(default,never):HTMLElement;
  var previousSibling(default,never):HTMLElement;
  var nextSibling(default,never):HTMLElement;
  var attributes(default, never):NamedNodeMap<Dynamic>;
  // Modified in DOM Level 2:
  var ownerDocument(default,never):HTMLDocument;
  // Modified in DOM Level 3:
  function insertBefore(newChild:HTMLElement, refChild:HTMLElement):HTMLElement;//raises(DOMException);
  // Modified in DOM Level 3:
  function replaceChild(newChild:HTMLElement, oldChild:HTMLElement):HTMLElement;//raises(DOMException);
  // Modified in DOM Level 3:
  function removeChild(oldChild:HTMLElement):HTMLElement;//raises(DOMException);
  // Modified in DOM Level 3:
  function appendChild(newChild:HTMLElement):HTMLElement;//raises(DOMException);
  function hasChildNodes():Bool;
  function cloneNode(deep:Bool):HTMLElement;
  // Modified in DOM Level 3:
  function normalize():Void;
  // Introduced in DOM Level 2:
  function isSupported(feature:String, version:String):Bool;
  // Introduced in DOM Level 2:
  var namespaceURI(default, never):String;
  // Introduced in DOM Level 2:
  var prefix:String;// raises(DOMException) on setting
  // Introduced in DOM Level 2:
  var localName:String;
  // Introduced in DOM Level 2:
  function hasAttributes():Bool;
  // Introduced in DOM Level 3:
  var baseURI(default, never):String;
  // Introduced in DOM Level 3:
  function compareDocumentPosition(other:HTMLElement):DocumentPosition;//raises(DOMException);
  // Introduced in DOM Level 3:
	var textContent:String;// raises(DOMException) on setting, raises(DOMException) on retrieval
  // Introduced in DOM Level 3:
  function isSameNode(other:HTMLElement):Bool;
  // Introduced in DOM Level 3:
  function lookupPrefix(namespaceURI:String):String;
  // Introduced in DOM Level 3:
  function isDefaultNamespace(namespaceURI:String):Bool;
  // Introduced in DOM Level 3:
  function lookupNamespaceURI(prefix:String):String;
  // Introduced in DOM Level 3:
  function isEqualNode(arg:HTMLElement):Bool;
  // Introduced in DOM Level 3:
  function getFeature(feature:String, version:String):DOMObject;
  // Introduced in DOM Level 3:
	function setUserData(key:String, data:DOMUserData, handler:UserDataHandler<HTMLElement>):DOMUserData;
  // Introduced in DOM Level 3:
	function getUserData(key:String):DOMUserData;
	//Document
	var documentElement(default,never):HTMLElement;
	function createElement(tagName:String):Dynamic;
	function createTextNode(str:String):Text;
	function getElementsByTagName(tagName:String):NodeList<HTMLElement>;
	//HTMLDocument
	var body(default, never):HTMLElement;
	var activeElement(default, never):HTMLElement;//TODO: patch Safari, Opera and older Firefox http://jamesgoodfellow.com/blog/post/documentactiveElement-in-Firefox---Finding-The-Element-That-Has-Focus.aspx
	var title:String;
	var URL(default,null):String;
	var referrer(default, never):String;
	var cookie(default, never):String;
	var compatMode(default, never):String;
	var defaultView(default, null):Void;
	function hasFocus():Bool;//TODO: patch IE
	function write(str:String):Void;
	function writeln(str:String):Void;
  function getElementsByName(elementName:String):NodeList<HTMLElement>;
  function getElementsByClassName(classNames:String):NodeList<HTMLElement>;
}