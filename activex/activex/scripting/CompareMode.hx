/**
 * ...
 * @author Cref
 */

package activex.scripting;

//virtual enum
class CompareMode {
	public static inline var binary:CompareMode = cast 0;
	public static inline var text:CompareMode = cast 1;
	public static inline var database:CompareMode = cast 2;
}