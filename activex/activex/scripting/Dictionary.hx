﻿/**
 * ...
 * @author Cref
 */

package activex.scripting;

import activex.Collection;

//@:native("AXO('Scripting.Dictionary')") extern class Dictionary<T> extends WritableAssociativeCollection<T> {
extern class Dictionary<T> extends WritableAssociativeCollection<T> {
	function new():Void;
  function add(key:String,value:T):Void;
  function items():VBArray<T>;
  function keys():VBArray<String>;
	function exists(key:String):Bool;
  var compareMode:CompareMode;
}

//virtual enum
class CompareMode {
	public static inline var binary:CompareMode = cast 0;
	public static inline var text:CompareMode = cast 1;
	public static inline var database:CompareMode = cast 2;
}