﻿package activex.scripting;

import activex.Collection;
import activex.TextStream;

extern class FileSystemObject {
	function new():Void;
	/*
	Properties
	*/
	//Returns a Drives collection consisting of all the Drive objects on a computer.
	var drives(default,null):Collection<Drive>;
	/*
	Methods
	*/
	//This method is used to append a name onto an existing path.
	function buildPath(path:String,name:String):String;
	//This method allows us to copy one or more files from one location (the source) to another (destination). 
	function copyFile(source:String,destination:String,?overwrite:Bool):Void;
	//Copies one or more folders and all contents, including files and subfolders, from one location to another.
	function copyFolder(source:String,destination:String,?overwrite:Bool):Void;
	//This method allows us to create a folder with the specified foldername.
	function createFolder(foldername:String):Folder;
	//Creates a text file and returns a TextStreamObject that can then be used to write to and read from the file.
	function createTextFile(filename:String,?overwrite:Bool,?unicode:Bool):TextStream;
	//This method deletes a specified file or files (using wilcards).
	function deleteFile(file:String,?force:Bool):Void;
	//This method deletes a specified folder, including all files and subfolders.
	function deleteFolder(folder:String,?force:Bool):Void;
	//This method lets us check if a specified drive exists. It returns True if the drive does exist and False if it doesn't.
	function driveExists(drive:String):Bool;
	//Lets us check whether a specified file exists. Returns True if the file does exist and False otherwise.
	function fileExists(file:String):Bool;
	//Allows us to check if a specified folder exists. Returns True if the folder does exist and False if it doesn't.
	function folderExists(folder:String):Bool;
	//This method gets the complete path from the root of the drive for the specified path string. 
	function getAbsolutePathName(path:String):String;
	//This method gets the base name of the file or folder in a specified path.
	function getBaseName(path:String):String;
	//This method returns a Drive object corresponding to the drive in a supplied path.
	function getDrive(drivename:String):Drive;
	//This method gets a string containing the name of the drive in a supplied path.
	function getDriveName(path:String):String;
	//Used to return a string containing the extension name of the last component in a supplied path.
	function getExtensionName(path:String):String;
	//Returns the File object for the specified file name.
	function getFile (filename:String):File;
	//This method is used to return the name of the last file or folder of the supplied path.
	function getFileName(path:String):String;
	//This method is used to return the version of the file in the specified path.
	function getFileVersion(path:String):String;
	//This method returns a Folder object of the folder specified in the folder parameter.
	function getFolder(foldername:String):Folder;
	//Returns a string containing the name of the parent folder of the last file or folder in a specified path.
	function getParentFolderName(path:String):String;
	//Returns the path to one of the special folders - \Windows, \System or \TMP.
	function getSpecialFolder(folderid:String):Folder;
	//This method is used to generate a random filename for a temporary file.
	function getTempName():String;
	//Moves one or more files from one location to another.
	function moveFile(source:String,destination:String):Void;
	//Moves one or more folders from one location to another.
	function moveFolder(source:String,destination:String):Void;
	//Opens the file specified in the filename parameter and returns an instance of the TextStreamObject for that file. )
	function openTextFile(filename:String,?iomode:Int,?create:Bool,?format:Int):TextStream;
}

typedef Drive = {
	/*
	Properties
	*/
	//Returns the amount of space available on the specified local or remote disk space.
	var availableSpace(default,null):Int;
	//Returns the dirve letter of the specified local or remote disk drive. Read only.
	var driveLetter(default,null):String;
	//Returns an integer indicating the type of the drive.
	var driveType(default,null):Int;
	//This property returns the file system type that is in use on the specified drive.
	var fileSystem(default,null):String;
	//Returns the amount of free space available to a user on the specified local or remote drive.
	var freeSpace(default,null):Int;
	//This property is a Boolean whose value is True if the specified drive is available for use and False otherwise.
	var isReady(default,null):Bool;
	//Returns the path for a specified file, folder or drive.
	var path(default,null):String;
	//Returns a Folder object that represents the root folder of the specified drive.
	var rootFolder(default,null):Folder;
	//Returns the decimal serial number for the specified drive. This number can be used to uniquely identify a disk volume.
	var serialNumber(default,null):Int;
	//Returns the network name in Universal Naming Convention (UNC) for the remote disk drive. Used only when working with a remote drive (DriveType property is 3).
	var shareName(default,null):String;
	//Returns the total space, in bytes, of the specified drive.
	var totalSize(default,null):Int;
	//Sets or returns the volume name of the specified drive.
	var volumeName:String;
}

typedef FileSystemEntity = {
	/*
	Properties
	*/
	//This property allows us to get or change the various attributes of a file.
	var attributes(default,null):Int;
	//This property gets the date and time that the file was created.
	var dateCreated(default,null):Date;
	//Gets the date and time that the file was last accessed
	var dateLastAccessed(default,null):Date;
	//This property returns the date and time that the file was last modified.
	var dateLastModified(default,null):Date;
	//Returns the drive letter of the drive where the file is located.
	var drive(default,null):String;
	//Lets us get or change the name of the specified file.
	var name:String;
	//This property gets the Folder object for the parent relating to the specified file.
	var parentFolder(default,null):Folder;
	//This property returns a file's path.
	var path(default,null):String;
	//Returns the short version of a filename (using the 8.3 convention). e.g. Employees.html is truncated to Employ~1.htm 
	var shortName(default,null):String;
	//Returns the short version of the file path (this is the path with any folder and file names truncated as above). 
	var shortPath(default,null):String;
	//Returns the size of a file in bytes.
	var size(default,null):Int;
	//Returns a string containing the file type description. e.g. For files ending in .TXT, "Text Document" is returned.
	var type(default,null):String;
	/*
	Methods
	*/
	//This method copies the selected file to the specified destination.
	function copy(destination:String,?overwrite:Bool):Void;
	//The method used to delete the file relating to the specified File object.
	function delete(?force:Bool):Void;
	//This method is used to move the file relating to the specified File object to a new destination.
	function move(destination:String):Void;
}

typedef File = {>FileSystemEntity,
	//This method opens a specified file and returns an instance of a TextStream object that can then be manipulated - read from, written or appended to.
	function openAsTextStream(?iomode:Int,?format:Int):TextStream;
}

typedef Folder = {>FileSystemEntity,	
	//Returns a Files collection consisting of all the File objects in the specified folder.
	var files(default,null):Collection<File>;
	//Returns a Boolean value: True if the folder is the root folder, and False otherwise.
	var isRootFolder(default,null):Bool;
	//This property returns a Folders collection that consists of all the folders in the specified folder.
	var subFolders(default,null):Collection<Folder>;
}