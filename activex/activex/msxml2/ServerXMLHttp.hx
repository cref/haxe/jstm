/**
 * http://support.microsoft.com/kb/290761
 * 
 * @author Cref
 */

package activex.msxml2;

extern class ServerXMLHttp extends XMLHttp {
  public function new():Void;
}