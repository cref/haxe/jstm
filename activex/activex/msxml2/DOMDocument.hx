/**
 * TODO: finish
 * TODO: implement standard DOM interfaces
 * @author Cref
 */

package activex.msxml2;

typedef IXMLDOMElement={
	var text:String;
	var dataType:String;
	var nodeTypedValue:Dynamic;
}

extern class DOMDocument/* implements org.w3c.dom.Document?*/ {
	public function new():Void;
	public function createElement(tagName:String):IXMLDOMElement;
}