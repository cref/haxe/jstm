package activex.internetexplorer;

//http://msdn.microsoft.com/en-us/library/aa752084(VS.85).aspx#
extern class Application {
	public function new():Void;
	//TODO: write and implement IE specific interfaces
	public var document(default, null):Dynamic;// org.w3c.dom.html.HTMLDocument;
	public function navigate(url:String, ?flags:Int, ?targetFrameName:String, ?postData:String, ?headers:String):Void;
	public function quit():Void;
}