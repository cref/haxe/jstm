/**
 * ...
 * @author Cref
 */
extern class SyntaxError extends Error<SyntaxError> {
	public function new(?message:String):Void;
}