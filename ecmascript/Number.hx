//exposing the constructor would be a really bad idea
extern class Number {
	static var NaN:Float;
	static var NEGATIVE_INFINITY:Float;
	static var POSITIVE_INFINITY:Float;
	static var MAX_VALUE:Float;
	static var MIN_VALUE:Float;
}