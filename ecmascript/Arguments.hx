typedef Arguments<T> = {>Object<Arguments<T>>,
	var length(default, never):Int;
	var callee(default, never):Function<Dynamic,Dynamic,Dynamic>;
}