/**
 * ...
 * @author Cref
 */
extern class ReferenceError extends Error<ReferenceError> {
	public function new(?message:String):Void;
}