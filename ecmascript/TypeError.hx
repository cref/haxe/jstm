/**
 * ...
 * @author Cref
 */
extern class TypeError extends Error<TypeError> {
	public function new(?message:String):Void;
}