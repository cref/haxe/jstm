/**
 * ...
 * @author Cref
 */

extern class RangeError extends Error<RangeError> {
	public function new(?message:String):Void;
}