/**
 * ...
 * @author Cref
 */
extern class EvalError extends Error<EvalError> {
	public function new(?message:String):Void;
}