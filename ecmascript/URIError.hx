/**
 * ...
 * @author Cref
 */
extern class URIError extends Error<URIError> {
	public function new(?message:String):Void;
}