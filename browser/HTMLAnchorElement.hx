/**
 * ...
 * @author Cref
 */

extern class HTMLAnchorElement extends HTMLElement {
  var href:String;
  var target:String;
}