/**
 * ...
 * @author Cref
 */

package google.maps;

extern class GeocoderLocationType {
	public static var APPROXIMATE:GeocoderLocationType;
	public static var GEOMETRIC_CENTER:GeocoderLocationType;
	public static var RANGE_INTERPOLATED:GeocoderLocationType;
	public static var ROOFTOP:GeocoderLocationType;
}