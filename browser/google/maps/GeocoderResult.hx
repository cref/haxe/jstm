/**
 * ...
 * @author Cref
 */

package google.maps;

typedef GeocoderResult = {
	address_components:Array<GeocoderAddressComponent>,
	formatted_address:String,
	geometry:GeocoderGeometry,
	types:Array<String>
}