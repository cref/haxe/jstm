/**
 * ...
 * @author Cref
 */

package google.maps;

typedef GeocoderAddressComponent = {
	long_name:String,
	short_name:String,
	geometry:GeocoderGeometry,
	types:Array<String>
}