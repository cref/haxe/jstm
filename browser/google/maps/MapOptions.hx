package google.maps;

typedef MapOptions = {
	//required fields
	zoom: Int,
	center: LatLng,
	mapTypeId: MapTypeId
	//haXe doesn't support optional typedef properties yet :(
	//backgroundColor:String etc
}