/**
 * ...
 * @author Cref
 */

package google.maps;

typedef MapPanes={
	mapPane:HTMLDivElement,
	overlayLayer:HTMLDivElement,
	overlayShadow:HTMLDivElement,
	overlayImage:HTMLDivElement,
	floatShadow:HTMLDivElement,
	overlayMouseTarget:HTMLDivElement,
	floatPane:HTMLDivElement
}