/**
 * ...
 * @author Cref
 */

package google.maps;

extern class Geocoder {
	public function new():Void;
	public function geocode(request:GeocoderRequest, callbck:Array<GeocoderResult>->GeocoderStatus->Void):Void;
}