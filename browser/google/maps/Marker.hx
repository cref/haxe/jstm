package google.maps;

extern class Marker extends MVCObject {
	public function new(?opts:MarkerOptions):Void;
}