/**
 * ...
 * @author Cref
 */

package google.maps;

typedef GeocoderRequest = {
	address:String,
	bounds:LatLngBounds,
	location:LatLng,
	region:String
}