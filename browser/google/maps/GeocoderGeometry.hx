/**
 * ...
 * @author Cref
 */

package google.maps;

typedef GeocoderGeometry = {
	bounds:LatLngBounds,
	location:LatLng,
	location_type:GeocoderLocationType,
	viewport:LatLngBounds
}