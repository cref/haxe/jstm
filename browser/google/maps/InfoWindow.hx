/**
 * http://code.google.com/intl/nl-NL/apis/maps/documentation/javascript/reference.html#InfoWindow
 * @author Cref
 */

package google.maps;

extern class InfoWindow extends MVCObject {
	public function new(?options:InfoWindowOptions):Void;
	public function open(?map:Map/*|StreetViewPanorama*/, ?anchor:MVCObject):Void;
	public function setPosition(position:LatLng):Void;
}