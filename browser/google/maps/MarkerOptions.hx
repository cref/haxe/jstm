package google.maps;

typedef MarkerOptions = {
	//required
	position:LatLng,
	//not required
	map:Map
}