/**
 * http://code.google.com/intl/nl-NL/apis/maps/documentation/javascript/basics.html
 * 
 * @author Cref
 */

package google.maps;

extern class Map extends MVCObject {
	public function new(elm:HTMLElement, ?options:MapOptions):Void;
	public function getCenter():LatLng;
	public function panTo(latLng:LatLng):Void;
}