/**
 * @author Cref
 */

package google.maps;

extern class MapTypeId {

	public static var HYBRID:MapTypeId;
	public static var ROADMAP:MapTypeId;
	public static var SATELLITE:MapTypeId;
	public static var TERRAIN:MapTypeId;
	
}