/**
 * ...
 * @author Cref
 */

package google.maps;

extern class GeocoderStatus {
	public static var ERROR:GeocoderStatus;
	public static var INVALID_REQUEST:GeocoderStatus;
	public static var OK:GeocoderStatus;
	public static var OVER_QUERY_LIMIT:GeocoderStatus;
	public static var REQUEST_DENIED:GeocoderStatus;
	public static var UNKNOWN_ERROR:GeocoderStatus;
	public static var ZERO_RESULTS:GeocoderStatus;
}