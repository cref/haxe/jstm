/**
 * ...
 * @author Cref
 */

extern class HTMLImageElement extends HTMLElement {
  var name:String;
  var align:String;
  var alt:String;
  var border:String;
  var width:Int;
  var height:Int;
  var hspace:Int;
  var vspace:Int;
  var isMap:Bool;
  var longDesc:String;
  var src:String;
  var useMap:String;
}