/**
 * ...
 * @author Cref
 */
import org.w3c.dom.TypeInfo;

extern class Attr extends Node<HTMLDocument,NodeList<HTMLElement>,HTMLElement,NamedNodeMap>/*, implements org.w3c.dom.Attr*/ {
  var name(default,never):String;
  var specified(default, never):Bool;
	var value:String;// raises(DOMException) on setting
  // Introduced in DOM Level 2:
  var ownerElement(default,never):HTMLElement;
  // Introduced in DOM Level 3:
  var schemaTypeInfo(default,never):TypeInfo;
  // Introduced in DOM Level 3:
  var isId(default,never):Bool;
}