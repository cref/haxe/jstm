/**
 * ...
 * @author Cref
 */

extern class HTMLScriptElement extends HTMLElement {

	public var src:String;
	public var async:Bool;
	public var onload:Dynamic->Void;
	
}