package jstm;

using ES5;
using hxtc.dom.DOMTools;//TODO: don't require DOMTools

class NonIE__ {
	
	function new(w:Window) untyped {
		ieMouseEvents(w);
	}
	
	//emulate IE mouseover and mouseout events, since they're very useful!
	static function ieMouseEvents(w:Window):Void {
		var d = w.document,lastEntered:HTMLElement = null,css = d.createStyleSheet();//TODO: create global patch stylesheet
		//we can safely use the pageBreakAfter property since it will never be used in combination with hover
		css.createStyle(':hover').pageBreakAfter = 'always';
		var dispEvt = function(e:HTMLElement,t:String,eo:MouseEvent) {
			//Firefox 3 and lower won't allow re-dispatching an event object so get a new one every time
			var evt:MouseEvent = d.createEvent('MouseEvent');
			evt.initMouseEvent('mouse'+t, false, false, null, null, eo.screenX, eo.screenY, eo.clientX, eo.clientY, eo.ctrlKey, eo.altKey, eo.shiftKey, eo.metaKey, eo.button, eo.relatedTarget);
			e.dispatchEvent(evt);
		}
		d.addEventListener('mouseover',function(eo:MouseEvent) {
			var e = lastEntered;
			while (e!=null && e.tagName!=null && w.getComputedStyle(e,null).pageBreakAfter != 'always') {
				untyped e._h = false;
				dispEvt(e,'leave',eo);
				e = e.parentNode;
			}
			e = lastEntered = eo.target;
			while (e!=null && !e._h) {
				untyped e._h = true;
				dispEvt(e,'enter',eo);
				e = e.parentNode;
			}
		},false);
	}
	
}