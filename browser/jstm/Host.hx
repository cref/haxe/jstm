/**
 * ...
 * @author Cref
 */

package jstm;

#if !macro
//make sure browserpatches will be compiled
import hxtc.web.Data;
import jstm.IE6__;
import jstm.Moz__;
//import jstm.Console__;
import jstm.DOMQueries__;
import jstm.DOMTraversal__;
import jstm.HashChange__;
import JSON__;
import ES5;

using ES5;
#end

class Host extends jstm.Runtime {
	
	static function __init__():Void {
		//the IE version reported by the browser
		ieVersion = Std.parseInt(window.navigator.appVersion.split('MSIE ')[1]);//TODO: QuirksMode (document.documentMode==5)
		//IE8 document mode can be IE7 or quirks: quirksmode should behave like IE7
		if (ieVersion > 7 && untyped !document.querySelector) ieVersion = 7;
		engine = ieVersion > 0
			?BrowserEngine.Trident//IE
			:untyped window.opera//Opera
				?BrowserEngine.Presto//Chrome, Safari etc.
				:(navigator.appVersion.indexOf('WebKit') > -1)
					?BrowserEngine.WebKit
					//TODO: verify if this check works for all Gecko-based browsers:
					:navigator.product == 'Gecko'
						?BrowserEngine.Gecko//Firefox, Netscape, Mozilla etc.
						:BrowserEngine.Unknown//you tell me!
		;
		//BROWSER PATCHING
		untyped {
			//make sure browserpatching is done before any code runs
			//make useType backup
			var uT = jstm.Runtime['useType'];
			jstm.Runtime.useType = function(tn:String, callbck:Dynamic->Void):Void {
				//run patches
					//restore useType
					jstm.Runtime.useType = uT;
					var browserPatch = 'jstm$'+(switch(Host.engine) {
						case BrowserEngine.Trident:'IE' + (ieVersion < 7?6:ieVersion);
						case BrowserEngine.Gecko:'Moz';
						default:'NonIE';
					}) + '__';
					var patchArgs = ['PATCH',browserPatch];
					if (!window.JSON) patchArgs.push('JSON__');
					//if (!window.console) patchArgs.push('jstm$Console__');
					if (!document.querySelector) patchArgs.push('jstm$DOMQueries__');
					//Browsermodus en Documentmodus zijn onafhankelijk van elkaar in te stellen
					//document.documentMode komt dus enkel overeen met documentmodus! nog eens goed naar kijken...
					if (untyped __js__("!('onhashchange' in window)") || document.documentMode < 8) patchArgs.push('jstm$HashChange__');
					jstm.Runtime.registerClass(new Function(patchArgs, 'return [{patches:arguments}]'));
					jstm.Runtime.useClass('PATCH', function(P) {
						var l = P.patches.length;
						for (p in 1...l) patches.push(P.patches[p]);
						//patch main window
						patchWindow(window);
						//run callback
						uT(tn, callbck);
					});
			}
		}
	}
	
	static function addAnimFr(w:Window) untyped {
		w.requestAnimationFrame =
			w.requestAnimationFrame			||
			w.webkitRequestAnimationFrame	||
			w.mozRequestAnimationFrame		||
			w.oRequestAnimationFrame		||
			w.msRequestAnimationFrame		||
			rqAnimFrEmu
		;
		w.cancelRequestAnimationFrame =
			//w.cancelAnimationFrame			||
			w.cancelRequestAnimationFrame		||
			w.webkitCancelRequestAnimationFrame	||
			w.mozCancelRequestAnimationFrame	||
			w.oCancelRequestAnimationFrame		||
			w.msCancelRequestAnimationFrame		||
			w.clearTimeout
		;
	}
	static var aniTimeout = 1000 / 30;//30 fps will do fine
	static var rqAnimFrEmu = untyped function(cb) return this.setTimeout(cb, aniTimeout);
	
	//static var srcRE:RegExp = untyped __js__("/jstm\\/Host/i");
	
	static var scriptElm:HTMLScriptElement;
	
	static function getTypeRegExp(path:Array<String>):RegExp {
		var check = '\n('+path.join('(?:\\.');
		for (i in 1...path.length) check += ')?';
		check += ')\n';
		return new RegExp(check,'');
	}
	
	static var mapsLoader = 'google.maps.Loader';
	//TODO: move outside Host class?
	function loadMaps() {
		untyped window.jstm.maps = function() {
			Runtime.registerType(mapsLoader, {});
			Runtime.setReadyState(mapsLoader,Runtime.READY);
		};
		//TODO: load api key from settings
		loadScript('//maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&sensor=true&callback=jstm.maps');
	}

	public override function loadType(typeName:String):Void {
		if (typeName == mapsLoader) return loadMaps();
		var path = typeName.split('.'),root=scrSrc[0];
		if (roots != null) {
			var matches=getTypeRegExp(path).exec(roots);
			if (matches != null) {
				var pack = matches[1];
				root = rootsObj.get(pack)+'/';
				path = typeName.split(pack + '.')[1].split('.');
			}
		}
		//var src = '';
		/*if (typeName == '_gat__init__') {
			//TODO: check if already loaded
			untyped _gaq = [function()jstm(function(_gat__init__){})];
			src = '//www.google-analytics.com/ga.js';
		}
		else {*/
			/*src = path[0] == 'google'
				?getGoogleSrc(path)
				:scriptElm.src.replace(srcRE, path.join('/').toLowerCase())
			;*/
			var src = (root + path.join('/') + scrSrc[1]);
			if (useLower) src=src.toLowerCase();
		//}
		//untyped console.log('loadType: '+src);
		loadScript(src);
		return;
	}
	
	/*function getGoogleSrc(path:Array<String>):String {
		return path.join('.');
	}*/
	
	//use HTML5 data attributes for arguments
	public static function getArgument(name:String):String {
		return scriptElm.getAttribute('data-'+name);
	}
	
	public static function loadScript(url:String, ?onload:Void->Void):HTMLScriptElement {
		//untyped console.log('loadScript: '+url);
		var d = window.document,h=d.documentElement.firstChild;
		var s:HTMLScriptElement = d.createElement('script');
		//uses setTimeout to force async requests for weird browsers (like BlackBerry)
		window.setTimeout(function() {
			s.src = url;
			s.onload = function(e) {
				h.removeChild(s);
				if (onload != null) onload();
			};
			//IE. TODO: implement as patch
			untyped s.onreadystatechange = function(e) {
				if (s.readyState == 'loading') return;
				s.onreadystatechange = null;
				if (onload != null) onload();
				//doesn't always work for some reason so we leave it
				//setTimeout(function() h.removeChild(s), 5000 );
			}
		}, 0);
		return h.appendChild(s);
	}
	
	static var n = 0;
	
	public static function loadCallback(url:String, callbck:Dynamic->Void, ?callbckStr:String = 'callback'):HTMLScriptElement {
		var id = n++;
		untyped window.jstm['_'+id] = function(o) {
			ES5.delete(window.jstm['_'+id]);
			callbck(o);
		}
		var s = loadScript(url+((url.indexOf('?')>-1)?'&':'?')+ES5.global.encodeURIComponent(callbckStr)+'=jstm._'+id);
		return s;
	}
	
	public static var domReady:Bool;
	
	public static function whenDOMReady(fn:Void->Void):Void domReady?fn():fnArr.push(fn)
	static var fnArr:Array<Void->Void>=[];
	static dynamic function onReady() {
		onReady = function() { };
		for (fn in fnArr) fn();
		domReady = true;
	}
	
	//whether to use all-lowercase url's for working nicely with SEO.
	//this is determined by checking the casing used for the initial url (that references the jstm.Host class)
	static var useLower:Bool;
	
	public function new() {
		super();
		//TODO: integrate Browser in Host (host.window etc.)
		var d = window.document;
		var allscr:NodeList<HTMLScriptElement> = cast d.getElementsByTagName('script');
		var hostIdRE:RegExp = untyped __js__("/jstm\\/Host/i"),tmp=null;
		//async support
		for (s in allscr) {
			tmp = s.src.split('#');
			var srcUrl = tmp[0];
			if (!hostIdRE.test(srcUrl)) continue;
			var match = hostIdRE.exec(srcUrl)[0];
			useLower = match == match.toLowerCase();
			scrSrc = srcUrl.split(match);//IE<8 skips empty results when using split with a RegExp so we use a string
			if (scrSrc.length > 1) {
				scriptElm = s;
				//when the async attribute is available, the DOM should already be fully loaded at this point
				domReady = s.async || d.readyState == 'complete';
				//interactive:	DOM can be used but not all parts of the document are ready yet
				//complete:		DOM can be used and all scripts that were included using script-tags are ready for use (same as DOMContentLoaded)
				//untyped alert(d.readyState);
				if (!domReady) {
					//when not async (or unsupported), DOMContentLoaded should still fire
					try {
						//all recent browsers that support addEventListener should also have DOMContentLoaded support
						d.addEventListener("DOMContentLoaded", function(e) onReady(), false);
					}
					catch (e:Dynamic) {
						d.attachEvent("onreadystatechange", function() if (d.readyState=='complete') onReady());
					}
				}
				break;
			}
		}
		//load package roots
		roots = getArgument('roots');
		if (roots != null) {
			//roots = 'nl.haaglanden= nl.haaglanden.controls=//controls.haaglanden.nl.vista8.test:88 cimple=//cimplecms.nl.vista8.test:88/cimple test=//test.com/jslib';
			rootsObj = Data.unserialize(roots, ' ', true);
			var rootsArr = ES5.getKeys(rootsObj);
			if (rootsArr.length == null) roots = null;
			else {
				//prepare for optimal performance: sort by length so we can instantly get the most specific match
				rootsArr.sort(function(a, b) return b.length - a.length);
				roots='\n'+rootsArr.join('\n')+'\n';
			}
		}
		var autoRun = tmp[1];
		if (autoRun != null) Runtime._runClass(autoRun);
	}
	var roots:String;
	var rootsObj:Dynamic<String>;
	
	static var patches:Array<Class<Void>> = [];
	static function patchWindow(w:Window) {
		addAnimFr(w);
		for (p in patches) untyped __new__(p, w);
	}
	
	var scrSrc:Array<String>;
	
	public static inline var window:Window = untyped __js__("window");
	//is determined in js.Boot but made accessible here
	public static var ieVersion(default,null):Int;
	//tells you if the browser is a mobile version
	//matches iPhone, iPad, IEMobile, Nokia S60, Samsung, Opera, RIM Blackberry, HP WebOS
	//not using EReg to keep initial footprint small
	public static var isMobile:Bool = untyped __js__("/(mobile|symbian|opera mini|blackberry|webos)/i").test(window.navigator.userAgent);
	
	//returns the browser engine (a.k.a. layout engine, a.k.a. render engine)
	public static var engine:BrowserEngine;
	
	//TODO: scriptEngine?
	
}

/**
 * virtual enum
 * TODO: generate using macro?
 * @author Cref
 */
class BrowserEngine {
	public static inline var Unknown:BrowserEngine = cast 0;//anything we couldn't succesfully detect
	public static inline var Trident:BrowserEngine = cast 1;//IE
	public static inline var WebKit:BrowserEngine = cast 2;//Chrome, Safari (based on KHTML, use same?)
	public static inline var Gecko:BrowserEngine = cast 3;//Firefox, Netscape, Mozilla etc.
	public static inline var Presto:BrowserEngine = cast 4;//Opera
}