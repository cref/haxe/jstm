/**
 * Emulates hashchange event on the window object.
 * Chrome 3 doesn't have native hashchange, Chrome 4 does.
 * 
 * notitie:
 * In IE kan steeds dezelfde event worden gedispatcht maar
 * Firefox (3.0 en lager) vereist een nieuwe event voor elke dispatch.
 * In IE werkt dit dan weer alleen als er al eens een hashchange event is aangemaakt.
 * Nog centraal oplossen.
 * 
 * @author Cref
 */
package jstm;

class HashChange__ {
	static function __init__():Void {
		if (untyped __js__("'onhashchange' in window&&document.documentMode>7")) throw new Error('incorrect usage');
	}
	
	function new(w:Window) {
		//trace('hashchange patch');
		var
			//l = w.location,
			//we use l.href and NOT l.hash because IE6 has a bug where '?' is not part of the fragment identifier
			//we use w.location and NOT l because BlackBerry browser doesn't give us an object reference :S
			getCurrentHash = function() return w.location.href.split('#').slice(1).join('#'),
			lastHash = getCurrentHash(),
			hashInterval = w.setInterval(function() {
				var h = getCurrentHash();
				if (h != lastHash) {
					//w.alert('hashchange dispatched');
					lastHash = h;
					//Firefox 3 and lower won't allow re-dispatching an event object so get a new one every time
					var e:Event = w.document.createEvent('Event');//should be HashChangeEvent
					e.initEvent('hashchange',false,false);
					w.dispatchEvent(e);//we ignore the fact that this event should also apply to the body element as it doesn't add anything
				}
			},120)
		;
	}
}