package jstm;

using hxtc.dom.DOMTools;
/*
 * TODO: hover, pseudo-classes, global CSS
 * */
class IE6__ extends IE7__ {
	
	public function new(w:Window) untyped {
		var d = w.document;
		//check out http://misterpixel.blogspot.com/2006/09/forensic-analysis-of-ie6.html
		d.execCommand("BackgroundImageCache", false, true);
		super(w);
	}
	
	override function patchDOM() {
		super.patchDOM();
		//emulate :hover
		d.documentElement.addEventListener('mouseover', overListener, false);
	}
	
	//TODO: add all css patches
	override function fixGlobalStyle(css:CSSStyleSheet) {
		untyped css.addRule('html','border:none');
		//untyped css.addRule('div.hover','border:2px solid red');//TESTING
		super.fixGlobalStyle(css);
	}
	
	private function hoverListener(e:MouseEvent) {
		var el:HTMLElement = e.currentTarget;
		el.switchClass('hover', e.type == 'mouseenter');
	}
	
	private function overListener(e:MouseEvent) {
		var el:HTMLElement = e.target;
		//we need this loop because mouseover will only fire for elements the mouse cursor can 'touch'
		while (el != e.currentTarget && !el.hasClass('hover')) {
			if (el.tagName != 'A' && untyped !el._hovers) {
				el.switchClass('hover', untyped el._hovers = true);
				el.addEventListener('mouseenter', hoverListener,false);
				el.addEventListener('mouseleave', hoverListener,false);
			}
			el = el.parentNode;
		}
	}
}