/**
 * ...
 * @author Cref
 */

extern class NamedNodeMap implements ArrayAccess<Attr>/*, implements org.w3c.dom.NamedNodeMap<Attr>*/ {
	function getNamedItem(name:String):Attr;
	function setNamedItem(arg:Attr):Attr;//raises(DOMException);
	function removeNamedItem(name:String):Attr;//raises(DOMException);
	function item(index:Int):Attr;
	var length(default,null):Int;
	// Introduced in DOM Level 2:
	function getNamedItemNS(namespaceURI:String, localName:String):Attr;// raises(DOMException);
	// Introduced in DOM Level 2:
	function setNamedItemNS(arg:Attr):Attr;// raises(DOMException);
	// Introduced in DOM Level 2:
	function removeNamedItemNS(namespaceURI:String, localName:String):Attr;// raises(DOMException);
	//Note: shows what a mess it takes to make an extern class iterable:
	var iterator(getIterator,null) : Void->Iterator<Attr>;
	private inline function getIterator():Dynamic {
		return untyped __js__("function(l){return function(){return Array.prototype.iterator.call(l)}}")(this);
	}
	//still not working! "Can't create closure on an inline extern method"
	//inline function iterator():Iterator<T> return new NodeListIterator(this)
}