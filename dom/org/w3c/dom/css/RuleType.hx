package org.w3c.dom.css;

class RuleType {
	public static inline var UNKNOWN_RULE             :RuleType = cast 0;
	public static inline var STYLE_RULE               :RuleType = cast 1;
	public static inline var CHARSET_RULE             :RuleType = cast 2;
	public static inline var IMPORT_RULE              :RuleType = cast 3;
	public static inline var MEDIA_RULE               :RuleType = cast 4;
	public static inline var FONT_FACE_RULE           :RuleType = cast 5;
	public static inline var PAGE_RULE                :RuleType = cast 6;
}