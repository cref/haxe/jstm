/**
 * ...
 * @author Cref
 */

package org.w3c.dom;

class OperationType {

  public static inline var      NODE_CLONED                   :OperationType = cast 1;
  public static inline var      NODE_IMPORTED                 :OperationType = cast 2;
  public static inline var      NODE_DELETED                  :OperationType = cast 3;
  public static inline var      NODE_RENAMED                  :OperationType = cast 4;
  public static inline var      NODE_ADOPTED                  :OperationType = cast 5;
	
}