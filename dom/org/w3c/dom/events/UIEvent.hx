/**
 * http://www.w3.org/TR/DOM-Level-3-Events/#events-Events-UIEvent
 * @author Cref
 */

package org.w3c.dom.events;

extern class UIEvent extends Event {
	/*
	readonly attribute views::AbstractView view;
	readonly attribute long            detail;
	void               initUIEvent(in DOMString typeArg, 
																 in boolean canBubbleArg, 
																 in boolean cancelableArg, 
																 in views::AbstractView viewArg, 
																 in long detailArg);
	// Introduced in DOM Level 3:
	void               initUIEventNS(in DOMString namespaceURIArg, 
																	 in DOMString typeArg, 
																	 in boolean canBubbleArg, 
																	 in boolean cancelableArg, 
																	 in views::AbstractView viewArg, 
																	 in long detailArg);
	*/
}