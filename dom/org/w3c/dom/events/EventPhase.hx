/**
 * ...
 * @author Cref
 */

package org.w3c.dom.events;

class EventPhase {
	
	public static inline var CAPTURING_PHASE:EventPhase = cast 1;
	public static inline var AT_TARGET:EventPhase = cast 2;
	public static inline var BUBBLING_PHASE:EventPhase = cast 3;
	
}