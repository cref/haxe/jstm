/**
 * ...
 * @author Cref
 */

package org.w3c.dom.events;

//typedef EventListener = Dynamic->Void;
typedef EventListener<T> = T->Void;