/**
 * http://www.w3.org/TR/DOM-Level-3-Core/core.html#i-Document
 * @author Cref
 */

package org.w3c.dom;

interface Document<TDoc,TNodeList,TElm,TNamedNodeMap> implements Node<TDoc,TNodeList,TElm,TNamedNodeMap> {
	var documentElement(default,never):TElm;
	function createElement(tagName:String):Dynamic;
	function createTextNode(str:String):Text;
	function getElementsByTagName(tagName:String):TNodeList;
}