/**
 * ...
 * @author Cref
 */

package org.w3c.dom.html;

typedef HTMLCollection = js.browser.DOM.NodeList<HTMLElement>;