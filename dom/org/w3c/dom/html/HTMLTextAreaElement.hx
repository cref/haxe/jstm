/**
 * ...
 * @author Cref
 */

package org.w3c.dom.html;

interface HTMLTextAreaElement<TDoc,TElm> implements HTMLElement<TDoc,TElm> {
	var name:String;
	var value:String;
}