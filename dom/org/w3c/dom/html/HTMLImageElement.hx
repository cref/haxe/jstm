/**
 * ...
 * @author Cref
 */

package org.w3c.dom.html;

interface HTMLImageElement<TDoc,TElm> implements HTMLElement<TDoc,TElm> {
  var name:String;
  var align:String;
  var alt:String;
  var border:String;
  var width:Int;
  var height:Int;
  var hspace:Int;
  var vspace:Int;
  var isMap:Bool;
  var longDesc:String;
  var src:String;
  var useMap:String;
}