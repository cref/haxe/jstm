/**
 * ...
 * @author Cref
 */

package org.w3c.dom;

class DerivationMethod {
	
  public static inline var       DERIVATION_RESTRICTION        :DerivationMethod = cast 0x00000001;
  public static inline var       DERIVATION_EXTENSION          :DerivationMethod = cast 0x00000002;
  public static inline var       DERIVATION_UNION              :DerivationMethod = cast 0x00000004;
  public static inline var       DERIVATION_LIST               :DerivationMethod = cast 0x00000008;
	
}