/**
 * http://www.w3.org/TR/DOM-Level-3-Core/core.html#DOMObject
 * 
 * @author Cref
 */

package org.w3c.dom;

typedef DOMUserData = Dynamic;