/**
 * http://www.w3.org/TR/DOM-Level-3-Core/core.html#ID-17189187
 * 
 * @author Cref
 */

package org.w3c.dom;

typedef DOMException = {
	code:ExceptionCode;
}