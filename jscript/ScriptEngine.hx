/**
 * groups all global functions inlined into one class
 * @author Cref
 */
class ScriptEngine {
	//haXe doesn't allow "static var name" so we use engineName
  public static inline var engineName:String = untyped ES5.global.ScriptEngine();
  public static inline var majorVersion:Int = untyped ScriptEngineMajorVersion();
  public static inline var minorVersion:Int = untyped ScriptEngineMinorVersion();
  public static inline var buildVersion:Int = untyped ScriptEngineBuildVersion();
	public static inline function toString():String return engineName + ' ' + majorVersion + '.' + minorVersion + ' build ' + buildVersion
	public static inline function getObject(objId:String):Dynamic return untyped __js__("GetObject")(objId)
	public static inline function collectGarbage():Void return untyped __js__("CollectGarbage")()
}