/**
 * TODO: write File class for js targets
 * @author Cref
 */

package js.io;

enum FileHandle {}

enum FileSeek {
	SeekBegin;
	SeekCur;
	SeekEnd;
}

class File {
	
	//shortcut
	private static var fs = new activex.scripting.FileSystemObject();
	
	public static function getContent(path:String):String {
		var f = fs.openTextFile(path,1,false,0);//TODO: use Stream for default utf-8 ?
		var r = f.atEndOfStream?'':f.readAll();
		f.close();
		return r;
	}
	
	/**
	 * only php.io.File in std lib offers this function.
	 * The documentation website says it should return an Int but in reality, it returns a String.
	 * I'm not returning anything until I know what to do
	 */
	public static function putContent(path:String, content:String) {
		//assumes overwrite mode
		var f = fs.openTextFile(path,2,true,0);//TODO: use Stream for default utf-8 ?
		f.write(content);
		f.close();
	}
	
	public static function copy(src, dst):Void fs.copyFile(src,dst,true)
	
	public static function write(path:String, binary:Bool):FileOutput {
		var s = new activex.adodb.Stream();
		//the standard classes are poorly documented: I don't know whether to open an existing file
		//and if so, wheter to set the position to the end of the stream, so.. I stop this right now
		//and I just throw an exception
		throw 'not yet implemented';
		return new haxe.io.FileOutput(s);
	}
	
}