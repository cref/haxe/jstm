/**
 * TODO: TEST! (untested)
 * @author Cref
 */

package js.io;

import haxe.io.Bytes;
import haxe.io.Error;

class FileOutput extends haxe.io.Output {
	
	//why should we use s:FileHandle ? :S
	public function new(s:activex.adodb.Stream) {
		stream = s;
	}
	
	var stream:activex.adodb.Stream;
	
	public override function flush() stream.flush()

	public override function close() stream.close()

	public override function writeByte(c:Int) stream.write([c])

	public override function writeBytes( s : Bytes, pos : Int, len : Int ) : Int {
		if ( pos < 0 || len < 0 || pos + len > s.length ) throw Error.OutsideBounds;
		var a = s.getData();
		if (pos != 0 || len != a.length) a = a.splice(pos, len);
		stream.write(a);
		return len;
	}
	
	public override function write( s : Bytes ) : Void stream.write(s.getData())
	public override function writeFullBytes( s : Bytes, pos : Int, len : Int ) writeBytes(s,pos,len)
	
}