/**
 * ...
 * @author Cref
 */

package js;

import activex.scripting.FileSystemObject;

class FileSystem {
	
	static var FS = new FileSystemObject();
	
	public static function exists(path:String):Bool {
		return FS.fileExists(path)||FS.folderExists(path);
	}
	public static inline function isDirectory(path:String):Bool {
		return FS.folderExists(path);
	}
	public static function kind(path:String):FileKind {
		return FS.fileExists(path)
			?FileKind.kfile
			:FS.folderExists(path)
				?FileKind.kdir
				:FileKind.kother('unknown')//input please?
		;
	}
	public static inline function createDirectory(dir:String):Void {
		FS.createFolder(dir);
	}
	public static inline function deleteDirectory(dir:String):Void {
		FS.deleteFolder(dir);
	}
	public static inline function deleteFile(file:String):Void {
		FS.deleteFile(file);
	}
	private static function getEntity(path:String):FileSystemEntity {
		return FS.fileExists(path)
			?cast FS.getFile(path)
			:FS.folderExists(path)
				?cast FS.getFolder(path)
				:throw 'path doesn\t exist'
		;
	}
	//should non-existing paths be created?
	public static inline function rename(path:String,newpath:String):Void {
		getEntity(path).move(newpath);
	}
	//should array be sorted? should names contain full paths?
	public static function readDirectory(path:String):Array<String> {
		var r = [];
		var folder = FS.getFolder(path);
		for (f in folder.subFolders) r.push(f.name);
		for (f in folder.files) r.push(f.name);
		return r;
	}
	public static inline function fullPath(path:String):String {
		return FS.getAbsolutePathName(path);
	}
	//http://haxe.org/api/neko/filestat NOT FINISHED!
	//something in there is slow...
	public static function stat(path:String):FileStat {
		var f = getEntity(path);
		return {
			gid : -1,
			uid : -1,
			atime : f.dateLastAccessed,
			mtime : f.dateLastModified,
			ctime : f.dateCreated,
			dev : -1,//f.drive
			ino : -1,
			nlink : -1,
			rdev : -1,
			size : f.size,
			mode : f.attributes //not checked if this is the same
		}
	}
}