/**
 * haxe.db.Sqlite wrapper for ADODB.
 * Requires Sqlite ODBC Driver:
 * http://www.ch-werner.de/sqliteodbc/
 * 
 * TODO: catch error when not installed and point to URL
 * 
 * @author Cref
 */

package js.db;

import js.db.ADOConnection;

class Sqlite {

	public static function open(file : String) : Connection {
		return ADOConnection.open('DSN=SQLite3 Datasource;Database='+file);
	}

}