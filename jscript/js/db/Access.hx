/**
 * Microsoft Access Connection.
 * Can be used for all Access versions.
 * 
 * Notes:
 * When using this class in a 64-bit environment from ASP,
 * you will need to configure IIS as 32-bit.
 * 
 * This class uses the Access Database Engine which works
 * with modern versions of Access and is backwards compatible
 * with the JET Database Engine.
 * 
 * Download: http://www.microsoft.com/downloads/details.aspx?familyid=7554f536-8c28-4598-9b72-ef94e038c891&displaylang=en
 * 
 * When you don't have this driver and you do not have permission to install it
 * you can still connect to older (2003) databases using forceJET.
 * 
 * @author Cref
 */

package js.db;

import js.db.Connection;

class Access {

	public static function open( file : String, ?forceJET:Bool ) : Connection {
		return ADOConnection.open('Provider=Microsoft.'+(forceJET?'Jet.OLEDB.4':'ACE.OLEDB.12')+'.0;Data Source=' + file);
	}

}