/**
 * Microsoft SQL Server Connection.
 * Can be used for all SQLServer versions including Express and Compact editions.
 * 
 * This class requires Microsoft SQL Server Native Client.
 * Download: http://www.microsoft.com/downloads/details.aspx?FamilyID=228de03f-3b5a-428a-923f-58a033d316e1&DisplayLang=en
 * 
 * @author Cref
 */

package js.db;

import js.db.Connection;

class SQLServer {

	public static function connect( params : ConnectionParameters ) : Connection {
		var str=hxtc.web.Data.serialize( {
			provider:'SQLNCLI10',
			server:params.host+(params.port==null?'':','+params.port),
			//option:131072,//package size?
			//stmt:null,//what's this?
			database:params.database,
			uid:params.user,
			pwd:params.pass
		},';', true);
		//trace(str);
		return ADOConnection.open(str);
	}
	
}