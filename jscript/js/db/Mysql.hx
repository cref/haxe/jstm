/**
 * haxe.db.Mysql wrapper for ADODB.
 * Requires MySQL ODBC Connector:
 * http://dev.mysql.com/downloads/connector/odbc/
 * 
 * TODO: catch error when not installed and point to URL
 * 
 * @author Cref
 */

package js.db;

import js.db.ADOConnection;

class Mysql {

	public static function connect( params : {
		host : String,
		port : Int,
		user : String,
		pass : String,
		socket : String,
		database : String
	} ) : Connection {
		return ADOConnection.open(hxtc.web.Data.serialize( {
			driver:'{mySQL ODBC 5.1 Driver}',
			server:params.host,
			port:params.port,
			//option:131072,//package size?
			//stmt:null,//what's this?
			database:params.database,
			uid:params.user,
			pwd:params.pass
		},';',true));
	}

}