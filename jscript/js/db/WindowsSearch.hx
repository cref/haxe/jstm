/**
 * Microsoft Windows Search Connection. (WSSQL)
 * 
 * shitty MSDN docs:
 * http://msdn.microsoft.com/en-us/library/bb231256(VS.85).aspx
 * 
 * incomplete property reference:
 * http://msdn.microsoft.com/en-us/library/bb419046(VS.85).aspx
 * these properties work too:
 * http://www.codeproject.com/KB/database/Indexing_Service_HOW-TO.aspx
 * 
 * TODO: add support for remote queries
 * 
 * @author Cref
 */

package js.db;

import js.db.Connection;

class WindowsSearch {

	public static function connect(/*TODO: ?hostname:String*/) : Connection {
		return ADOConnection.open('provider=Search.CollatorDSO.1;extended properties="Application=Windows"');
	}
	
}