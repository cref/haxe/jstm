/*
 * Microsoft Indexing Service Connection.
 * 
 * http://www.codeproject.com/KB/database/Indexing_Service_HOW-TO.aspx
 * http://msdn.microsoft.com/en-us/library/ms691971(VS.85).aspx
*/
package js.db;

import js.db.Connection;

class IndexingService {

	public static function connect(indexName:String, ?host:String) : Connection {
		var ds = indexName;
		return ADOConnection.open('provider=MSIDXS;Data Source="'+(host==null?'':host+'.')+ds+'"');
	}
	
}