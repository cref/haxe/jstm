/**
 * TODO: fully support some hxtc js targets: WSH, Rhino, JScript,NET, JSDB etc.
 * @author Cref
 */
package js;

#if jscript
import activex.wscript.Shell;
import activex.wscript.Network;
#end

#if (js || flash)
class Sys {//TODO
	public static function time():Float {
		return Date.now().getTime() / 1000;//same as haxe.Timer.stamp
		//return flash.Lib.getTimer() / 1000;//is there any difference?
	}
	#if jscript
	public static inline function getCwd():String {
		return Shell.currentDirectory;
	}
	public static inline function setCwd(dir:String):Void {
		Shell.currentDirectory=dir;
	}
	public static inline function systemName():String {
		return Network.computerName;
	}
	public static inline function sleep(msec:Float):Void {
		#if js_wsh
		js.wsh.WScript.sleep(msec);
		#else
		if (msec < 1000 || msec / 1000 % 1 > 0) throw 'please use a multiple of 1000';
		//it works... but only with whole seconds
		Shell.popup('', untyped msec / 1000);
		//TODO: support msecs using activex.wscript.Shell.run('cscript.exe eval.js "WScript.sleep(123)"') ?
		#end
	}
	#end
}
#elseif neko
typedef Sys = neko.Sys;
#elseif cpp
typedef Sys = cpp.Sys;
#elseif php
typedef Sys = php.Sys;
#end