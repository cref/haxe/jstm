/**
 * Initializes the JScript platform
 * This is used by ASP, HTA and WSH
 * @author Cref
 */

package jstm;

class JScriptHost__ extends Runtime {

	public function new() {
		super();
		//a global function that gets called whenever an activex object is created
		//must be a function returning a function because haxe adds generates: new AXO("my.Object")()
		untyped AXO = function(id:String) return function() return new ActiveXObject(id);
	}
	
}