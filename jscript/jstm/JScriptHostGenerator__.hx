/**
 * 
 * @author Cref
 */

package jstm;

#if macro
import haxe.macro.Type;

class JScriptHostGenerator__ extends Generator {
	
	/**
	 * translate extern classes in the activex package for JScript targets
	 * @param	c
	 * @return
	 */
	override function getNativeName(c:BaseType):String {
		if (c.pack[0] == 'activex') {
			c.pack.shift();
			return 'AXO("' + super.getNativeName(c) + '")';
		}
		return super.getNativeName(c);
	}
	
}
#end