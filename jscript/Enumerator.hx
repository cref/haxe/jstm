/**
 * ...
 * @author Cref
 */
import activex.Collection;

//Please use iterators instead of using Enumerator directly
extern class Enumerator < T > {
	function atEnd():Bool;
	function moveNext():Void;
	function item():T;
	function new(enumerable:Collection<T>):Void;
}