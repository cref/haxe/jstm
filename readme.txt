This repository contains a number of haxe libraries:

- activex
  a library that contains activex types. Note that these are in a seperate library
  because there's also other javascript platforms than jscript (like GLUEScript and JSDB)
  that have access to activex. These platform libraries should rely on this library as well.

- asp
  Library for writing ASP programs for Microsoft's Internet Information Server (IIS).
  Implements haXe standard API webserver classes including remoting.

- browser
  A JSTM-based alternative for the default haXe js target. Now that the browser is not the
  only supported javascript platform anymore, all browser-related code that's still in the
  haXe standard library should really be moved to a library since browser types are now by
  default available to all js platforms which isn't correct. Other than that, it's support
  is far from complete. This is my attempt at providing a better browser target. I've tried
  implementing the DOM on extern classes. Browser abstraction should be done automatically
  by the jstm.Platform class.

- dom
  A first attempt at translating the massive W3C specification to haXe. The W3C interfaces
  are placed into a package structure as defined by the standard. This is not part of the
  browser library because these types should be available to any target/platform that offers
  DOM features. This includes activex but also Java (Rhino) and even Flash (DOM Events).
  My hopes are that the community will work together to come to a complete W3C DOM library
  that's as flexible and cross-target as possible without losing too much type safety.

- ecmascript
  This library contains extern classes for native ECMAScript objects that are not part of
  the haXe standard library and a helper class for accessing native global functions. This
  class is not part of the jstm library since Flash is also ECMAScript-based.
  Planned to support all ES5 features.
  In my opinion, haXe should be completely ES5 compatible by default.

- hta
  HTML Application library. For writing Internet Explorer only web applications that
  have access to ActiveScript features.

- jscript
  A library that contains all types that are shared by all ActiveScript-based libraries,
  to be exact: asp, hta and wsh.
  Implements part of the haXe standard API: database access and filesystem.
  and adds some extra database connections (Access, SQLServer, IndexingService and WindowsSearch)

- jstm
  The JavaScript Type Manager library. Use this library to build new javascript platform
  libraries upon. It contains generator and runtime base classes.

- wsh
  The Windows Scripting Host for writing (interactive) shell scripts





Still to come are:
air, dotnet, gluescript, jsdb, jslibs, lov8, rhino, v8cgi and more.
If you have the need for any of these js platforms, please let me know!
