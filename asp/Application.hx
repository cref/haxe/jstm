import activex.Collection;

extern class Application {
	public static function lock():Void;
  public static function unlock():Void;
  public static function value(key:String):Dynamic;//should only return base types
  public static var contents(default,null):WritableAssociativeCollection<Dynamic>;
  public static var staticObjects(default,null):WritableAssociativeCollection<Dynamic>;
}
