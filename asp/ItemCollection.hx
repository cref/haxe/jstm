import activex.Collection;
import jstm.Host;

extern class ItemCollection<T> extends AssociativeCollection<T> {
  inline function exists(key:String):Bool {
		return Host.exists(this,key);
  }
}