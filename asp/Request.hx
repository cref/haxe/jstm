import activex.ByteArray;

extern class Request {
  public static var totalBytes:Int;
  public static var clientCertificate:ItemCollection<ReadItem>;//klopt niet helemaal want er kunnen ook dates worden geretourneerd maar voorlopig houden we t hierop
  public static var cookies:ItemCollection<RequestCookie>;
  public static var form:ItemCollection<ReadItem>;
  public static var queryString:ItemCollection<ReadItem>;
  public static var serverVariables:ItemCollection<ReadItem>;
  public static function binaryRead(count:Int):ByteArray;//zie haxe.io package voor eventuele aansluiting hierop
}

private typedef ReadItem = {
	var item(default, null):String;
}

typedef RequestCookie = {>ReadItem,
  //is read-only and allows pages on a domain made up of more than one server to share cookie information.
  var domain(default,null):String;
  //is read-only and is the date on which the cookie expires. Unless a date is specified, the cookie will expire when the session ends. If a date is specified, the cookie will be stored on the client's disk after the session ends. 
  var expires(default,null):Date;
  //is read-only and uses Boolean values to specify whether the cookie contains keys. 
  var hasKeys(default,null):Bool;
  //is read-only and if set, the cookie is sent by this path. If this argument is not set, the default is the application path. 
  var path(default,null):String;
  //is read-only and indicates if the cookie is secure. 
  var secure(default,null):Bool;
  //is read-only and returns the value for the cookie. 
  //var item(default,null):String;
}