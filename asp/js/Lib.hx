/**
 * ...
 * @author Cref
 */

package js;

class Lib {

	/**
		Print the specified value on the default output.
	**/
	public static inline function print( v : Dynamic ) : Void Response.write(v)

	/**
		Print the specified value on the default output followed by a newline character.
	**/
	public static inline function println( v : Dynamic ) : Void print(v+"\n")
	
}