import activex.Collection;

extern class Session {
  public static function abandon():Void;
  public static var codePage:Int;
  public static var lcId:Int;
  public static var sessionId(default,null):Int;
  public static var timeout:Int;
  public static function value(key:String):Dynamic;//moet eigenlijk alleen standaard types retourneren maar weet niet hoe ik dat kan bepalen
  public static var contents(default,null):WritableAssociativeCollection<Dynamic>;
  public static var staticObjects(default,null):WritableAssociativeCollection<Dynamic>;
}