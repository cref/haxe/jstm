 /*
 The Server object provides access to the utility functions of the server.
 */
extern class Server {
 /*
 Properties
 */
  //The ScriptTimeout property is the amount of runtime in seconds for a script before it terminates.
  public static var scriptTimeout:Int;
 /*
 Methods
 */
  //The CreateObject method creates an instance of an object to use in an Active Server Page.
  public static function createObject(objectId:String):Dynamic;
  //The Execute method allows you to call another ASP page from inside an ASP page. When the called ASP page completes its tasks, you are then returned to the calling ASP page.
  public static function execute(path:String):Void;
  //The GetLastError method returns an ASPError object that describes any pre-processing, runtime, or script compiling errors that occurred.
  public static function getLastError():ASPError;
  //The HTMLEncode method applies HTML syntax to a specified string of ASCII characters.
  public static function htmlEncode(s:String):String;
  //The MapPath method maps a relative or virtual path to a physical path.
  public static function mapPath(path:String):String;
  //The Transfer method allows you to transfer all of the state information for all of the built-in objects from one ASP page to another. Unlike the Execute method, when the ASP page that you have transferred to is finished, you do not return the original ASP page.
  public static function transfer(path:String):Void;
  //The URLEncode method applies URL rules to a specified string of ASCII characters.
  public static function urlEncode(s:String):String;
}