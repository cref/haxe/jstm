/**
 * ...
 * @author Cref
 */

package jstm;

class Host extends JScriptHost__ {
	//emulates exists on ItemCollections
	public static function exists(c:ItemCollection<Dynamic>,key:String):Bool {
		return key!=null && c.item(key)!=c.item(' ~DUMMY`');
	}
}