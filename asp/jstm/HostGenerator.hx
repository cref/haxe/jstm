/**
 * ...
 * @author Cref
 */

package jstm;

#if macro
import haxe.macro.JSGenApi;

using StringTools;

class HostGenerator extends JScriptHostGenerator__ {

	override public function writeSplit(folderPath:String, ext:String):Array<String> {
		var filenames = super.writeSplit(folderPath, ext);
		writeFile(folderPath + '/' + mainClassName + /*#if debug '.debug'+ #end*/ '.asp',
			includeTemplate[0] + filenames.join(includeTemplate[1] + '\n' + includeTemplate[0]) + includeTemplate[1]+
			(options.mainCall!=false?'\n<script language="jscript" runat="server">jstm("' + mainClassName + '")</script>':'')
		);
		return filenames;
	}
	
	static var includeTemplate=['<script language="jscript" runat="server" src="','"></script>'];
	
}
#end