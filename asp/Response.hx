extern class Response {
 /*
 Properties
 */
  //The Buffer property tells whether the page output being sent to the browser is buffered.
  public static var buffer:Bool;
  //The CacheControl property determines whether a proxy server can cache the Active Server Page.
  public static var cacheControl:String;//enum: Public of Private in een wrapper class (webserver platform)
  //The Charset property appends the name of the character set being used to the content-type header contained in the response object.
  public static var charset:String;
  //This property allows you to set the code page for a response, which allows you to define how characters are encoded in different languages.
  public static var codePage:Int;
  //The ContentType property specifies the HTTP content type/subtype for the response header.
  public static var contentType:String;
  //The Expires property specifies the length of time in minutes until a cached page on the browser expires.
  public static var expires:Int;
  //The ExpiresAbsolute property specifies a date and time when a cached page on the browser will expire.
  public static var expiresAbsolute:Date;
  //The IsClientConnected property indicates whether the browser has disconnected from the server since the last Response.Write.
  public static var isClientConnected(default,null):Bool;
  //This property can be used to get or set the Locale Identifier, which determines for specific geographic locales the formatting of dates, times, and currencies.
  public static var lcId:String;//ook weer een enum kandidaat
  //The Status property specifies the value of the status line returned by the server.
  public static var status:String;
 /*
 Methods
 */
  //The AddHeader method adds a new named HTTP header with a specific value to the response.
  public static function addHeader(name:String,value:String):Void;
  //The AppendToLog method adds a string to the end of an entry in the Web server log for this request.
  public static function appendToLog(s:String):Void;
  //The BinaryWrite method sends specific data to the current HTTP output without any character conversions.
  public static function binaryWrite(data:Dynamic):Void;//niet ByteArray?
  //The Clear method clears (erases) any buffered HTML output.
  public static function clear():Void;
  //The End method causes the web server to stop processing the script and to return the current results without processing the rest of the file.
  public static function end():Void;
  //The Flush method sends the contents of the buffer.
  public static function flush():Void;
  //adds a value to the pics-label response header.
  public static function pics(value:String):Void;
  //The Redirect method tries to connect the browser to a different URL.
  public static function redirect(url:String):Void;
  //The Write method sends any specified variant to the browser.
  public static function write(s:Dynamic):Void;
 /*
 Collections
 */
  //Syntax: Response.Cookies(Name)[(Key)|.Attribute]=Value
  //The Cookies collection property allows you to add a cookie to a browser and add values to the cookie.
  public static var cookies(default,null):ItemCollection<ResponseCookie>;//attribute laten we voorlopig buiten beschouwing
}

private typedef WriteItem = {
	var item(null, default):String;
}

typedef ResponseCookie = {>WriteItem,
  //is write-only and allows pages on a domain made up of more than one server to share cookie information.
  var domain(null,default):String;
  //is write-only and is the date on which the cookie expires. Unless a date is specified, the cookie will expire when the session ends. If a date is specified, the cookie will be stored on the client's disk after the session ends. 
  var expires(null,default):Date;
  //is read-only and uses Boolean values to specify whether the cookie contains keys. 
  var hasKeys(default,null):Bool;
  //is write-only and if set, the cookie is sent by this path. If this argument is not set, the default is the application path. 
  var path(null,default):String;
  //is write-only and indicates if the cookie is secure. 
  var secure(null,default):Bool;
  //is write-only and sets the value for the cookie. 
  //var item(null,default):String;
}