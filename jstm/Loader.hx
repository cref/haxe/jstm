/**
 * This interface should be implemented by jstm.Host
 * 
 * in jstm.Runtime.main:
 * host:Loader=new Host();
 * 
 * in jstm.Runtime.useType:
 * host.loadType(typeName);
 */
package jstm;

interface Loader {
	public function loadType(typeName:String):Void;
}