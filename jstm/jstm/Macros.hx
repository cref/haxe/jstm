package jstm;
#if macro
import haxe.macro.Expr;
import haxe.macro.Context;

class Macros {
	
	public static function getClassName(e:Expr):String {
		//trace(e.expr);
		return switch(Context.typeof(e)) {
			case TType(ref, _):
				ref.toString().split('#').join('');
			default:
				Context.error('type expected', e.pos);
		}
	}
	
	public static function stringExpr(s:String):Expr {
		return {expr:stringExprDef(s),pos:Context.currentPos()};
	}
	
	public static function stringExprDef(s:String):ExprDef {
		return ExprDef.EConst(Constant.CString(s));
	}
	
	public static function getClassNameExpr(e:Expr):Expr {
		return stringExpr(getClassName(e));
		//return {expr:stringDef(getClassName(e)),pos:e.pos};
	}
	
}
#end