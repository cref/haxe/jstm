package js;

enum FileKind {
	kdir;
	kfile;
	kother( k : String );
}